import rospy
import numpy as np
from prob_grasp_planner.srv import ArmKDL, ArmKDLRequest

class CollisionChecker():
    '''
    This class abstracts calls to KDL for the arm, checks for collisions etc.
    '''
    def __init__(self, epsilon = 0.0):
        self.arm_links_fk_service_name = "arm_links_fk"
        self.arm_jacobians_service_name = "arm_links_jacobians"
        self.fingers_fk_service_name = "fingers_links_fk"
        self.all_links_fk_service_name = "all_links_fk"
        self.spheres_radius = {}
        self.spheres_diameters = {}

        self.epsilon = epsilon

        self.initialize_arm_spheres()
        self.initialize_hand_spheres()
        for sphere in self.spheres_radius:
            r = self.spheres_radius[sphere]
            self.spheres_diameters[sphere] = 2*r

        self.arm_links_names = [str(i) for i in range(2,8)]
        self.arm_links_names += ["palm_link"]
        self.arm_links_names += ["allegro_mount"] 
        self.arm_links_names += ["virtual_palm_center"] 
        self.arm_links_names = set(self.arm_links_names)

        self.to_obj_frame_fn = None
        self.box_x = None
        self.box_y = None
        self.box_z = None
        self.box_corner_1 = None

    def update_box_dim(self,x,y,z, buff=0.015):
        assert x>0 and y>0 and z>0
        self.box_x = x + buff
        self.box_y = y + buff
        self.box_z = z + buff
        # this is object box
        self.box_corner_1 = np.array([self.box_x/2,self.box_y/2,self.box_z/2])
        # this is reachability box
        fingers_length = 0.16 #m
        self.bounding_box_corner = self.box_corner_1 + fingers_length

    def visualize_collision_spheres(self, q, vis_client):
        fks = self.fk_all_links(q)
        vis_client.update_collision_spheres(fks, self.spheres_diameters)
        rospy.loginfo("Spheres visualized")


    def get_arm_links_names(self):
        return self.arm_links_names

    def initialize_hand_spheres(self):
        fingers = ["index", "middle", "ring", "thumb"]
        frames = []
        for finger in fingers:
            for i in range(0,4):
                frames += [finger + "_link_" + str(i)]

        # all these values are diameters of spheres obtained by "eye inspection"
        for frame in frames:
            if "link_0" in frame:
                self.spheres_radius[frame] = 0.0
            else:
                self.spheres_radius[frame] = 0.037 + self.epsilon

        frames = []
        for finger in fingers:
            frames += [finger + "_biotac_origin"]
        for frame in frames:
            self.spheres_radius[frame] = 0.015 + self.epsilon
 
        for finger in fingers:
            for i in range(1,4):
                if finger == "thumb" and i == 1:
                    continue
                frame = finger + "_virtual_sphere_" + str(i)
                self.spheres_radius[frame] = 0.035 + self.epsilon
                if i == 3:
                    self.spheres_radius[frame] = 0.025 + self.epsilon
 
            for i in range(1,3):
                frame = finger + "_biotac_virtual_sphere_" + str(i)
                self.spheres_radius[frame] = 0.015 + self.epsilon

        frame="thumb_virtual_sphere_3_offset"
        self.spheres_radius[frame] = 0.045 + self.epsilon

        # radius is half a diameter
        for frame in self.spheres_radius:
            self.spheres_radius[frame] /= 2


    def initialize_arm_spheres(self):
        # dictionary keys: lbr4_x_link -> x
        for i in range(2,7):
            self.spheres_radius[str(i)] = 0.195 + self.epsilon

        self.spheres_radius[str(7)] = 0.11 + self.epsilon
        self.spheres_radius["palm_link"] = 0.0 + self.epsilon
        self.spheres_radius["allegro_mount"] = 0.1 + self.epsilon
        self.spheres_radius["virtual_palm_center"] = 0.14 + self.epsilon

    def fk_arm_links(self,q):
        assert len(q) == 7,len(q)
        resp = call_service(self.arm_links_fk_service_name, ArmKDLRequest(q, None, None), ArmKDL)
        return parse_arm_links_resp(resp)
    
    def fk_hand_links(self,q):
        assert len(q) == 7+16,len(q)
        resp = call_service(self.fingers_fk_service_name, ArmKDLRequest(q[:7], q[7:], None), ArmKDL)
        return parse_hand_links_resp(resp)

    def fk_all_links(self,q):
        assert len(q) == 7+16,len(q)
        resp = call_service(self.all_links_fk_service_name, ArmKDLRequest(q[:7], q[7:], None), ArmKDL)
        arm_fk = parse_arm_links_resp(resp)
        fingers_fk = parse_hand_links_resp(resp)

        all_fk = arm_fk
        all_fk.update(fingers_fk)
        return all_fk

    def get_Jacobians(self,q):
        '''
        Returns jacobian for lbr4_link_2 .. 7 in the arm and the palm link.
        Returns dictionary with key being link number / "palm".
        '''
        resp = call_service(self.arm_jacobians_service_name, ArmKDLRequest(q, None, None), ArmKDL)
        J = {}

        J_flat = resp.jacobian_link_2
        J["2"] = np.reshape(np.array(J_flat), (6,2))

        J_flat = resp.jacobian_link_3
        J["3"] = np.reshape(np.array(J_flat), (6,3))

        J_flat = resp.jacobian_link_4
        J["4"] = np.reshape(np.array(J_flat), (6,4))

        J_flat = resp.jacobian_link_5
        J["5"] = np.reshape(np.array(J_flat), (6,5))

        J_flat = resp.jacobian_link_6
        J["6"] = np.reshape(np.array(J_flat), (6,6))

        J_flat = resp.jacobian_link_7
        J["7"] = np.reshape(np.array(J_flat), (6,7))

        J_flat = resp.jacobian
        J["palm_link"] = np.reshape(np.array(J_flat), (6,7))

        J_flat = resp.jacobian_allegro_mount
        J["allegro_mount"] = np.reshape(np.array(J_flat), (6,7))

        J_flat = resp.jacobian_virtual_palm_center
        J["virtual_palm_center"] = np.reshape(np.array(J_flat), (6,7))
        return J

    def point_in_collision_with_object(self,point_obj_frame):
        return point_in_collision_with_object_box(point_obj_frame, self.box_corner_1)

    def q_in_collision_with_object_box(self,q):
        fks = self.fk_all_links(q)
        for link in fks:
            pose = fks[link] # world frame
            pose_obj_frame = self.to_obj_frame_fn(pose)
            point_obj_frame = pose_obj_frame[:3]

            # center of the sphere for fast fail
            if point_in_collision_with_object_box(point_obj_frame, self.box_corner_1):
                #print(str(link) + "in collision with object box (center of sphere)")
                return True

            # whole sphere
            if cube_intersects_sphere(
                    self.box_corner_1,
                    point_obj_frame,
                    self.spheres_radius[link]):
                #print(str(link) + "in collision with object box (some part of the sphere)")
                return True

        return False


    def compute_sdf_collision(self, q, sdf_and_gradient_fn, debug=False):
        assert len(q) == 7 + 16
        fks = self.fk_all_links(q)
        gradients_in_contact = {}
        sdfs_in_contact = {}
        max_penetration_link = None
        min_sdf = None
        for link in fks:
            pose = fks[link] # world frame
            pose_obj_frame = self.to_obj_frame_fn(pose)
            point_obj_frame = pose_obj_frame[:3]
            sdf_before_sphere, gradient = sdf_and_gradient_fn(point_obj_frame)
            sdf = sdf_before_sphere - self.spheres_radius[str(link)] 
            if debug:
                print("link: ", link)
                print("point in obj frame", point_obj_frame)
                print("sphere radius", self.spheres_radius[link])
                print("sdf before sphere: ", str(sdf_before_sphere))
                print("sdf[m]: ", str(sdf))
                print("----------------------------")

            gradient += 1e-3
            gradient /= np.linalg.norm(gradient)
            gradient *= abs(sdf) 
            if sdf < 0.0:
                gradients_in_contact[link] = gradient
            if min_sdf is None or sdf < min_sdf:
                min_sdf = sdf
                max_penetration_link = link
        return gradients_in_contact, max_penetration_link, min_sdf

    def get_heights(self,q, arm_only=False):
        if arm_only:
            fks = self.fk_arm_links(q[:7])
        else:
            fks = self.fk_all_links(q)
        heights = {}
        for link_name in fks:
            heights[link_name] = fks[link_name][2]

        return heights

    def get_lowest_link(self,q, arm_only=False):
        heights = self.get_heights(q, arm_only)
        min_value = None
        min_key = None

        for key in heights:
            if key == 2:
                continue
            value = heights[key]
            if min_key == None:
                min_key = key
                min_value = value
            if value < min_value:
                min_key = key
                min_value = value
        return min_key, min_value - self.spheres_radius[str(min_key)]

    def get_penetration_depth(self, sdf, q):
        '''
        Returns penetration and the links that penetrates the most.
        If there is no penetration, return negative value.
        If there is penetration, return positive value (depth).
        '''
        link, height = self.get_lowest_link(q)
        return link, sdf(height)

    def object_reachable_from_the_point(self, point):
        return point_in_collision_with_object_box(point, self.bounding_box_corner)

def cube_sdf(point, box_corner_1):
    d = abs(point) - box_corner_1
    return min(max(d[0],max(d[1],d[2])),0) +\
            np.linalg.norm([
                max(d[0],0.0),
                max(d[1],0.0),
                max(d[2],0.0)])


# https://stackoverflow.com/questions/4578967/cube-sphere-intersection-test
def cube_intersects_sphere(C1, S, R):
    sdf = cube_sdf(S, C1)
    return sdf - R <= 0

def point_in_collision_with_object_box(point, corner):
    return abs(point[0]) < corner[0] and abs(point[1]) < corner[1] and abs(point[2]) < corner[2]

def parse_hand_links_resp(resp):
    fks = {}

    for item in resp.fingers_poses:
        link = item.key
        pose = item.value
        fks[link] = pose

    return fks

def parse_arm_links_resp(resp):
    fks = {}
    fks["2"] = resp.link_2_pose
    fks["3"] = resp.link_3_pose
    fks["4"] = resp.link_4_pose
    fks["5"] = resp.link_5_pose
    fks["6"] = resp.link_6_pose
    fks["7"] = resp.link_7_pose
    fks["palm_link"] = resp.pose
    fks["virtual_palm_center"] = resp.virtual_palm_center_pose
    fks["allegro_mount"] = resp.allegro_mount_pose

    return fks

def call_service(name, request, message_type, wait_for_service=False):
    response = None
    try: 
        if wait_for_service:
            wait_for_service(name)
        proxy = rospy.ServiceProxy(name, message_type)
        response = proxy(request)
    except Exception as e:
        rospy.logerr("Service " + name + " call failed: " + str(e))
    rospy.loginfo("Service " + name + " call finished successfully.")
    return response

def wait_for_service(service_name):
    rospy.loginfo("Waiting for service " + service_name)
    rospy.wait_for_service("Calling service" + service_name)
    rospy.loginfo("Waiting for service " + service_name + " finished successfully.")

def run_lowest_link_test():
    q_arm = [-0.22,1.66,0.65,-0.82,-0.03,-0.19,0.25]
    q_hand = [0.0 for i in range(16)]
    q = q_arm + q_hand
    cc = CollisionChecker()
    link, height = cc.get_lowest_link(q, arm_only=True)
    assert link == "palm_link", link
    assert height - 0.5 < 1e-6, height
    print("Lowest link test passed.")

def run_sdf_test():
    q_arm = [0.47775983, 1.4400457, -0.70614521, -1.28975023, 0.8575789, -0.32561686, 1.8940689]
    q_hand = [-0.18949510169918946, 0.012995781031213575, 0.0, 0.0, -0.12367542220764564, 0.010511545216423227, 0.0, 0.0, -0.037266251629719055, -0.10039748784942928, 0.0, 0.0, 1.4601129447176657, 1.4601129447176657, 0.0, 0.0]
    q = q_arm + q_hand

    cc = CollisionChecker()
    table_height = 0.6
    sdf = lambda robot_height: table_height - robot_height

    # penetration positive when in collision
    link, penetration = cc.get_penetration_depth(sdf, q)
    assert penetration > 0, penetration

    # penetration negative if not in collision
    for i in range(7):
        q[i] = 0.0
    link, penetration = cc.get_penetration_depth(sdf, q)
    assert penetration < 0, penetration
    print("SDF test passed.")


def main():
    run_lowest_link_test()
    run_sdf_test()
    cc = CollisionChecker()
    print(cc.get_arm_links_names())

if __name__ == "__main__":
    main()
                 
