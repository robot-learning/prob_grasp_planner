#!/usr/bin/env python

from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics
from prob_grasp_planner.srv import ArmKDL, ArmKDLResponse
from prob_grasp_planner.msg import ROSMap
from ll4ma_util.ros_util import pose_to_homogeneous
import numpy as np
import rospy
import PyKDL


class GraspKDL():
    '''
    Grasp KDL class.
    '''

    def __init__(self):
        rospy.init_node('grasp_kdl')
        self.robot = URDF.from_parameter_server()
        self.base_link = 'world'
        self.end_link = 'palm_link'
        self.kdl_kin = KDLKinematics(self.robot, self.base_link, self.end_link)
        self.ik_solver = None
        fk_service = rospy.Service("arm_fk", ArmKDL, self.forward_service)
        jacobian_service = rospy.Service("arm_jacobian", ArmKDL, self.jacobian_service)
        jacobians_service = rospy.Service("arm_links_jacobians", ArmKDL, self.arm_links_jacobian_service)
        ik_service = rospy.Service("arm_ik", ArmKDL, self.ik_service)
        self.allegro_mount_kdl = KDLKinematics(self.robot, self.base_link, "allegro_mount")
        self.virtual_palm_center_kdl = KDLKinematics(self.robot, self.base_link, "virtual_palm_center")
        '''
        self.link_7_kdl = KDLKinematics(self.robot, self.base_link, "iiwa_link_7")
        self.link_6_kdl = KDLKinematics(self.robot, self.base_link, "iiwa_link_6")
        self.link_5_kdl = KDLKinematics(self.robot, self.base_link, "iiwa_link_5")
        self.link_4_kdl = KDLKinematics(self.robot, self.base_link, "iiwa_link_4")
        self.link_3_kdl = KDLKinematics(self.robot, self.base_link, "iiwa_link_3")
        self.link_2_kdl = KDLKinematics(self.robot, self.base_link, "iiwa_link_2")
        '''
        self.link_7_kdl = KDLKinematics(self.robot, self.base_link, "lbr4_7_link")
        self.link_6_kdl = KDLKinematics(self.robot, self.base_link, "lbr4_6_link")
        self.link_5_kdl = KDLKinematics(self.robot, self.base_link, "lbr4_5_link")
        self.link_4_kdl = KDLKinematics(self.robot, self.base_link, "lbr4_4_link")
        self.link_3_kdl = KDLKinematics(self.robot, self.base_link, "lbr4_3_link")
        self.link_2_kdl = KDLKinematics(self.robot, self.base_link, "lbr4_2_link")

        fingers = ["index", "middle", "ring", "thumb"]
        frames = []
        for finger in fingers:
            for i in range(0,4):
                frames += [finger + "_link_" + str(i)]
        for finger in fingers:
            frames += [finger + "_biotac_origin"]
        for finger in fingers:
            for i in range(1,4):
                if finger == "thumb" and i == 1:
                    continue
                frames += [finger + "_virtual_sphere_" + str(i)]
                if i == 3:
                    frames += ["thumb_virtual_sphere_3_offset"]
            for i in range(1,3):
                frames += [finger + "_biotac_virtual_sphere_" + str(i)]
        self.fingers_kdl = {}
        for frame in frames:
            self.fingers_kdl[frame] = KDLKinematics(self.robot, "world", frame)

        self.all_links_fk_s = rospy.Service("all_links_fk", ArmKDL, self.all_links_fk_service)
        self.arm_links_fk_s = rospy.Service("arm_links_fk", ArmKDL, self.arm_links_fk_service)
        self.fingers_links_fk_s = rospy.Service("fingers_links_fk", ArmKDL, self.fingers_fk_service)

    def all_links_fk_service(self, req):
        resp = self.fingers_fk_service(req)
        resp = self.arm_links_fk_service(req, resp)
        return resp

    def fingers_fk_service(self, req):
        hand_q = req.hand_q
        arm_q = req.arm_q
        poses = {}
        for key in self.fingers_kdl:
            if "index" in key:
                q = arm_q + hand_q[:4]
            elif "middle" in key:
                q = arm_q + hand_q[4:8]
            elif "ring" in key:
                q = arm_q + hand_q[8:12]
            elif "thumb" in key:
                q = arm_q + hand_q[12:]
            else:
                raise Exception("what?")
            for i in range(4):
                if "_link_" + str(i) in key or "_virtual_sphere_" + str(i) in key:
                    if "biotac" not in key:
                        q = q[:7+i+1]
                        break
            T = self.fingers_kdl[key].forward(q) 
            pose = extract_pose(T)
            poses[key] = pose
        ros_maps = []
        for key in poses:
            ros_map = ROSMap(key, poses[key])
            ros_maps.append(ros_map)
        resp = ArmKDLResponse()
        resp.fingers_poses = ros_maps
        return resp
        
    def arm_links_fk_service(self, req, response=None):
        T_2 = self.link_2_kdl.forward(req.arm_q[:2])
        T_3 = self.link_3_kdl.forward(req.arm_q[:3])
        T_4 = self.link_4_kdl.forward(req.arm_q[:4])
        T_5 = self.link_5_kdl.forward(req.arm_q[:5])
        T_6 = self.link_6_kdl.forward(req.arm_q[:6])
        T_7 = self.link_7_kdl.forward(req.arm_q)
        T_palm = self.forward(req.arm_q)
        T_allegro_mount = self.allegro_mount_kdl.forward(req.arm_q)
        T_virtual_palm_center = self.virtual_palm_center_kdl.forward(req.arm_q)

        if response is None:
            response = ArmKDLResponse()

        response.pose = extract_pose(T_palm)
        response.link_2_pose = extract_pose(T_2)
        response.link_3_pose = extract_pose(T_3)
        response.link_4_pose = extract_pose(T_4)
        response.link_5_pose = extract_pose(T_5)
        response.link_6_pose = extract_pose(T_6)
        response.link_7_pose = extract_pose(T_7)
        response.allegro_mount_pose = extract_pose(T_allegro_mount)
        response.virtual_palm_center_pose = extract_pose(T_virtual_palm_center)

        return response

    def arm_links_jacobian_service(self, req):
        J_2 = self.link_2_kdl.jacobian(req.arm_q[:2])
        J_3 = self.link_3_kdl.jacobian(req.arm_q[:3])
        J_4 = self.link_4_kdl.jacobian(req.arm_q[:4])
        J_5 = self.link_5_kdl.jacobian(req.arm_q[:5])
        J_6 = self.link_6_kdl.jacobian(req.arm_q[:6])
        J_7 = self.link_7_kdl.jacobian(req.arm_q)
        J_palm = self.jacobian(req.arm_q)
        J_allegro_mount = self.allegro_mount_kdl.jacobian(req.arm_q)
        J_virtual_palm_center = self.virtual_palm_center_kdl.jacobian(req.arm_q)


        response = ArmKDLResponse()
        response.jacobian = np.squeeze(J_palm.flatten()).tolist()[0]
        response.jacobian_link_2 = np.squeeze(J_2.flatten()).tolist()[0]
        response.jacobian_link_3 = np.squeeze(J_3.flatten()).tolist()[0]
        response.jacobian_link_4 = np.squeeze(J_4.flatten()).tolist()[0]
        response.jacobian_link_5 = np.squeeze(J_5.flatten()).tolist()[0]
        response.jacobian_link_6 = np.squeeze(J_6.flatten()).tolist()[0]
        response.jacobian_link_7 = np.squeeze(J_7.flatten()).tolist()[0]
        response.jacobian_allegro_mount = np.squeeze(J_allegro_mount.flatten()).tolist()[0]
        response.jacobian_virtual_palm_center = np.squeeze(J_virtual_palm_center.flatten()).tolist()[0]
        return response

    def forward_service(self, req):
        T = self.forward(req.arm_q)

        pose=np.zeros(6)
        R=PyKDL.Rotation(T[0,0],T[0,1],T[0,2],T[1,0],T[1,1],T[1,2],T[2,0],T[2,1],T[2,2])
        pose[3:6]=R.GetRPY()
        pose[0:3]=T[0:3,3].ravel()

        response = ArmKDLResponse()
        response.pose = pose
        return response

    def jacobian_service(self, req):
        jacobian = self.jacobian(req.arm_q)
        response = ArmKDLResponse()
        J_flat = np.squeeze(jacobian.flatten()).tolist()[0]
        response.jacobian = J_flat
        return response

    def ik_service(self, req):
        q_ik = self.inverse(req.pose_ik.pose)
        if q_ik is None:
            q_ik = []
        response = ArmKDLResponse()
        response.q_ik = q_ik
        return response

    def forward(self, q):
        pose = self.kdl_kin.forward(q)
        return pose

    def jacobian(self, q):
        J = self.kdl_kin.jacobian(q)
        return J

    def inverse(self, pose):
        T = pose_to_homogeneous(pose)
        q = self.kdl_kin.inverse_search(T, timeout=3.)
        if q is None:
            rospy.logerr('No IK solution for grasp planning!')
        return q


def extract_pose(T):
    pose=np.zeros(6)
    R=PyKDL.Rotation(T[0,0],T[0,1],T[0,2],T[1,0],T[1,1],T[1,2],T[2,0],T[2,1],T[2,2])
    pose[3:6]=R.GetRPY()
    pose[0:3]=T[0:3,3].ravel()
    return pose


if __name__=="__main__":
    arm_kdl = GraspKDL()
    rospy.spin()

