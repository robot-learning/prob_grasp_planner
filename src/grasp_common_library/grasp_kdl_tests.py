#!/usr/bin/env python

from urdf_parser_py.urdf import URDF
from pykdl_utils.kdl_kinematics import KDLKinematics
from prob_grasp_planner.srv import *
from grasp_kdl import GraspKDL
from ll4ma_util.ros_util import get_pose_stamped_msg
import numpy as np
import rospy
import PyKDL


def test_jacobian_serialization(arm_kdl):
    q = np.random.rand(7)
    J_1 = arm_kdl.jacobian(q)
    req = ArmKDLRequest(q, None, None)
    J_flat = arm_kdl.jacobian_service(req).jacobian
    J_2 = np.reshape(np.array(J_flat), (6,7))
    assert np.allclose(J_1,J_2)
    rospy.loginfo("Jacobian serialization is correct.")

def test_FK_determinism(arm_kdl):
    q = np.random.rand(7)
    req = ArmKDLRequest(q, None, None)
    pose_1 = arm_kdl.forward_service(req).pose
    pose_2 = arm_kdl.forward_service(req).pose
    assert np.allclose(pose_1, pose_2)
    rospy.loginfo("FK is deterministic")

def test_FK_correctness(arm_kdl):
    q = np.array([0.0 for i in range(7)])
    q[1] = 0.22
    req = ArmKDLRequest(q, None, None)
    pose_computed = arm_kdl.forward_service(req).pose
    pose_expected = np.array([0.009, -0.220, 1.928, 0.216, 0.001, -0.008])
    assert np.allclose(pose_computed, pose_expected,atol=1e-2), print("is /robot_description correctly loaded?")
    rospy.loginfo("FK is correct")

def test_Jacobian_correctness(arm_kdl):
    '''
    Tests that Jacobian pseudoinverse decreases error in EE displacement.
    '''
    q = np.random.rand(7)
    pose = np.array(arm_kdl.forward_service(ArmKDLRequest(q,None,None)).pose)
    q_target = np.copy(q)
    q_target[0] -= 0.3
    pose_target = np.array(arm_kdl.forward_service(ArmKDLRequest(q_target,None,None)).pose)
    assert not np.allclose(pose, pose_target)
   
    err = np.linalg.norm(pose - pose_target)
    for i in range(20):
        J_flat = arm_kdl.jacobian_service(ArmKDLRequest(q, None, None)).jacobian
        J = np.reshape(np.array(J_flat), (6,7))
        q = q + 0.1*np.dot(np.linalg.pinv(J), pose_target - pose)
        pose = np.array(arm_kdl.forward_service(ArmKDLRequest(q, None, None)).pose)
        err_new = np.linalg.norm(pose - pose_target)
        if err_new < 1e-4:
            break
        assert err_new <= err
        err = err_new
    rospy.loginfo("Jacobian is correct.")

def test_links_FK_correctness(arm_kdl):
    q = np.array([0.0 for i in range(7)])
    q[1] = 0.22
    q[4] = -0.01
    req = ArmKDLRequest(q, None, None)
    resp = arm_kdl.arm_links_fk_service(req)
    link_2_pose_expected = np.array([0.004,0.001,0.920,0.0,-0.216,1.57])
    link_3_pose_expected = np.array([0.003,-0.040,1.107,0.0,-0.216,1.57])
    link_4_pose_expected = np.array([0.001,-0.084,1.311,0.0,-0.216,1.57])
    link_5_pose_expected = np.array([0.001,-0.125,1.498,0.002,-0.216,1.563])
    link_6_pose_expected = np.array([0.001,-0.168,1.692,0.002,-0.216,1.563])
    link_7_pose_expected = np.array([0.001,-0.185,1.768,0.002,-0.216,1.563])
    palm_pose_expected = np.array([0.009,-0.220,1.928,0.216,0.001,-0.008])

    assert np.allclose(resp.link_2_pose, link_2_pose_expected, atol=1e-2)
    assert np.allclose(resp.link_3_pose, link_3_pose_expected, atol=1e-2)
    assert np.allclose(resp.link_4_pose, link_4_pose_expected, atol=1e-2)
    assert np.allclose(resp.link_5_pose, link_5_pose_expected, atol=1e-2)
    assert np.allclose(resp.link_6_pose, link_6_pose_expected, atol=1e-2)
    assert np.allclose(resp.link_7_pose, link_7_pose_expected, atol=1e-2)
    assert np.allclose(resp.pose, palm_pose_expected, atol=1e-2)
    rospy.loginfo("FK each link in the arm is correct")

def test_IK(arm_kdl):
    '''
    Compute IK for a given pose and check that FK for the result results in initial pose.
    '''
    q = np.random.rand(7)
    req = ArmKDLRequest(q, None, None)
    target_pose = arm_kdl.forward_service(req).pose
    R = PyKDL.Rotation.RPY(target_pose[3], target_pose[4], target_pose[5])
    target_quaternion = R.GetQuaternion()
    pose_msg = get_pose_stamped_msg(target_pose[:3], target_quaternion)

    req = ArmKDLRequest(None, None, pose_msg)
    resp = arm_kdl.ik_service(req)
    q_computed = resp.q_ik
    req = ArmKDLRequest(q_computed, None, None)
    computed_pose = arm_kdl.forward_service(req).pose
    assert np.allclose(target_pose, computed_pose, atol=1e-3)
    rospy.loginfo("IK is correct")

def test_Jacobians_correctness(arm_kdl):
    '''
    Tests that Jacobian pseudoinverse decreases error in a random link (here link 3).
    '''
    q = np.random.rand(7)

    req = ArmKDLRequest(q, None, None)
    resp = arm_kdl.arm_links_fk_service(req)
    link_3_pose = resp.link_3_pose

    q_target = np.copy(q)
    q_target[0] -= 0.1

    req = ArmKDLRequest(q_target, None, None)
    resp = arm_kdl.arm_links_fk_service(req)
    link_3_target_pose = resp.link_3_pose
    assert not np.allclose(link_3_pose, link_3_target_pose)
   
    pose = link_3_pose
    pose_target = link_3_target_pose
    err = np.linalg.norm(pose - pose_target)
    for i in range(2):
        J_flat = arm_kdl.arm_links_jacobian_service(ArmKDLRequest(q, None, None)).jacobian_link_3
        J = np.reshape(np.array(J_flat), (6,3))
        q[:3] = q[:3] + 0.1*np.dot(np.linalg.pinv(J), pose_target - pose)
        req = ArmKDLRequest(q_target, None, None)
        resp = arm_kdl.arm_links_fk_service(req)
        pose = resp.link_3_pose
        err_new = np.linalg.norm(pose - pose_target)
        if err_new < 1e-4:
            break
        assert err_new < err, i
        err = err_new
    rospy.loginfo("Jacobian for link 3 is correct.")

def test_fingers_FK(kdl):
    arm_q = [0.0 for i in range(7)]
    arm_q[1] = 0.26
    hand_q = [0.0 for i in range(16)]
    hand_q[1] = 0.14
    resp = kdl.fingers_fk_service(ArmKDLRequest(arm_q, hand_q, None))
    index_link_3_pose_expected = np.array([0.022, -0.237, 2.034, 0.168, 0.133, 0.021])
    for item in resp.fingers_poses:
        key = item.key
        value = item.value
        if key == "index_link_3":
            pose = value
            break

    assert np.allclose(pose, index_link_3_pose_expected, atol=1e-2), pose


if __name__=="__main__":
    arm_kdl = GraspKDL()
    rospy.loginfo("------ Test results  ------")
    test_jacobian_serialization(arm_kdl)
    test_FK_correctness(arm_kdl)
    test_FK_determinism(arm_kdl)
    test_IK(arm_kdl)
    test_Jacobian_correctness(arm_kdl)
    test_links_FK_correctness(arm_kdl)
    test_Jacobians_correctness(arm_kdl)
    rospy.loginfo("All arm tests passed successfully.")
    kdl = arm_kdl
    test_fingers_FK(kdl)
    rospy.loginfo("Hand tests passed successfully.")
