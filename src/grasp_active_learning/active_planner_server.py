#!/usr/bin/env python
import roslib
import rospy
from prob_grasp_planner.srv import *
# from grasp_active_learner import GraspActiveLearner 
from grasp_active_learner_fk import GraspActiveLearnerFK 
import numpy as np
import time
from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped
import roslib.packages as rp
import sys
pkg_path = rp.get_pkg_dir('prob_grasp_planner')
sys.path.append(pkg_path + '/src/grasp_common_library')
import grasp_common_functions as gcf
import align_object_frame as align_obj
from data_proc_lib import DataProcLib 
import show_voxel
import tensorflow as tf
sys.path.append(rp.get_pkg_dir("grasp_pipeline"))
from grasp_pipeline.srv import *


PALM_DOF = 6

class ActivePlanServer:

    def __init__(self):
        rospy.init_node('active_plan_server')
        self.vis_preshape = rospy.get_param('~vis_preshape', False)
        virtual_hand_parent_tf = rospy.get_param('~virtual_hand_parent_tf', '')
        active_models_path = rospy.get_param('~active_models_path', '')

        self.data_proc_lib = DataProcLib()
        self.cvtf = gcf.ConfigConvertFunctions(self.data_proc_lib)

        #prior_name = 'GMM' 
        prior_name = 'MDN'

        g1 = tf.Graph()
        with g1.as_default():
            self.active_planner = GraspActiveLearnerFK(prior_name, 
                                        active_models_path=active_models_path,
                                        cvtf=self.cvtf) 
        self.action = 'grasp_suc'

        # the last object
        self.object_voxelized = None
        self.object_size = None
        # the associated result
        self.grasp_configuration = None # [palm_pose, fingers_configuration] [6,8]

    def handle_predict_grasp_success(self, req):
        fingers_configuration = [req.index_joint_0,
                                 req.index_joint_1,
                                 req.middle_joint_0,
                                 req.middle_joint_1,
                                 req.ring_joint_0,
                                 req.ring_joint_1,
                                 req.thumb_joint_0,
                                 req.thumb_joint_1]
        self.grasp_configuration[PALM_DOF:] = fingers_configuration # self.grasp_configuration is updated here
        result = self.active_planner.pred_clf_suc_prob(self.grasp_configuration,
                                                       self.object_voxelized,
                                                       self.object_size)
        response = PredictGraspSuccessResponse()
        response.success_probability = result
        return response


    def handle_get_fingers_configuration_gradient(self, req):
        # obtain gradient for the whole grasp configuration: palm pose and fingers
        # grasp configuration is the configuration that was used the last time when predicting success
        palm_fingers_gradient, _ = self.active_planner.compute_clf_config_grad(
                                          self.grasp_configuration,
                                          self.object_voxelized,
                                          self.object_size)
        # extract fingers gradient
        selected_joints_gradient = palm_fingers_gradient[PALM_DOF:] # first two joints for every finger

        all_joints_gradient = [selected_joints_gradient[0], selected_joints_gradient[1], 0.0, 0.0, # index
                               selected_joints_gradient[2], selected_joints_gradient[3], 0.0, 0.0, # middle
                               selected_joints_gradient[4], selected_joints_gradient[5], 0.0, 0.0, # ring
                               selected_joints_gradient[6], selected_joints_gradient[7], 0.0, 0.0] # thumb
        response = FingersGradientResponse()
        response.gradients = all_joints_gradient
        return response
                                                                
        
    def handle_grasp_active_plan(self, req):
        response = GraspPreshapeResponse()

        sparse_voxel_grid, voxel_size, voxel_grid_full_dim = self.data_proc_lib.voxel_gen_client(req.obj)
        
        #show_voxel.plot_voxel(sparse_voxel_grid) 
        
        voxel_grid = np.zeros(tuple(voxel_grid_full_dim))
        voxel_grid_index = sparse_voxel_grid.astype(int)
        voxel_grid[voxel_grid_index[:, 0], voxel_grid_index[:, 1], 
                    voxel_grid_index[:, 2]] = 1
        voxel_grid = np.expand_dims(voxel_grid, -1)
        self.object_voxelized = voxel_grid

        obj_size = np.array([req.obj.width, req.obj.height, req.obj.depth])
        self.object_size = obj_size

        planner = self.active_planner

        print("starting planning")
        reward, ik_config_inf, config_inf, obj_val_inf, \
            ik_config_init, config_init, obj_val_init, plan_info, inf_time = \
            planner.grasp_strategies(self.action, voxel_grid,
                                            obj_size)
        self.grasp_configuration = config_inf

        print("planning done!")
        if reward is None:
            print 'Active planning failed!'
            return response

        #object_pose_tf_frame = 'object_pose'
        object_pose_tf_frame = 'estimated_object_pose'
        full_config_init = self.cvtf.convert_preshape_to_full_config(
                                            config_init, object_pose_tf_frame)
        full_config_inf = self.cvtf.convert_preshape_to_full_config(
                                            config_inf, object_pose_tf_frame)

        self.data_proc_lib.update_palm_pose_client(full_config_inf.palm_pose)

        print("config_inf:")
        print(str(config_inf))
        print("full_config_inf:")
        print(str(full_config_inf))
        print("ik config inf:")
        print(str(ik_config_inf))

        response.allegro_joint_state = [full_config_inf.hand_joint_state] # joint_state message
        response.arm_configuration = ik_config_inf[:7] # array of doubles
        print("response: ")
        print(response)
        return response


    def create_active_plan_server(self):
        '''
            Create grasps active learning server.
        '''
        rospy.Service('grasp_active_plan', GraspPreshape, 
                        self.handle_grasp_active_plan)
        rospy.Service('predict_grasp_success', PredictGraspSuccess, self.handle_predict_grasp_success)
        rospy.Service('get_fingers_configuration_gradient', FingersGradient, self.handle_get_fingers_configuration_gradient)

        rospy.loginfo('Ready to perform grasp active planning.')


if __name__ == '__main__':
   active_planner = ActivePlanServer() 
   active_planner.create_active_plan_server()
   rospy.spin()

