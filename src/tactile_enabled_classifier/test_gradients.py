import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from dnn_architecture_preshape import PreshapeNet
from mdn import MDN
from planner import Planner


def main():
    test_gradients()

def test_gradients()
    model = PreshapeNet()
    model.load_state_dict(torch.load("/home/mmatak/dnn-preshape"))
    mdn = MDN()
    mdn = mdn.to(mdn.device)
    mdn.load_state_dict(torch.load("/home/mmatak/mdn-preshape"))
    planner = Planner(model, mdn)
    prior_relative_error = 0.0
    prior_absolute_error = 0.0
    likelihood_relative_error = 0.0
    likelihood_absolute_error = 0.0
    num_dimensions = 8 + 6 # 8 for joints, 6 for palm pose

    voxel_np = np.load("test-voxel-grid.npy")
    object_dim_np = np.array([0.162,0.162,0.162])

    # test network gradients
    for i in range(num_dimensions):
        continue
        eps = 1e-3
        gradient_index = i

        d = np.zeros(num_dimensions)
        d[gradient_index] = 1
        q = np.random.rand(num_dimensions)
        q_plus = q + eps * d
        q_minus = q - eps * d

        # likelihood gradient test
        print("likelihood gradient test")
        f1 = planner.evaluate_log_likelihood(q_plus, voxel_np, object_dim_np)[1]
        f2 = planner.evaluate_log_likelihood(q_minus, voxel_np, object_dim_np)[1]
        grad_fd = (f1-f2) / (2 * eps)
        print("grad FD: ", grad_fd)

        planner.evaluate_log_likelihood(q, voxel_np, object_dim_np)
        autodiff_grad = np.copy(planner.q_grad)
        print("autodiff grad: ", autodiff_grad[gradient_index])

        want = grad_fd
        got = autodiff_grad[gradient_index]
        err = (want - got) / (want)
        print("relative err: ", err)
        print("abs err: ", abs(want-got))
        likelihood_relative_error += err
        likelihood_absolute_error += abs(want-got)

        print()

        # prior gradient test
        d = torch.zeros(num_dimensions).to("cuda:0")
        d[gradient_index] = 1
        print("testing prior gradient")
        q, (pi, mu, L, L_d) = planner.mdn.sample_np(voxel_np, object_dim_np)
        q_plus = q + eps * d
        q_minus = q - eps * d

        f1 = planner.mdn.loss_fn(pi, mu, L, L_d, q_plus)
        f2 = planner.mdn.loss_fn(pi, mu, L, L_d, q_minus)
        grad_fd = (f1-f2) / (2 * eps)
        print("grad FD: ", grad_fd)

        planner.mdn.loss_fn(pi, mu, L, L_d, q, save_input_gradient=True)
        autodiff_grad = np.copy(planner.mdn.q_grad)
        print("autodiff grad: ", autodiff_grad[gradient_index])

        want = grad_fd
        got = autodiff_grad[gradient_index]
        err = (want - got) / (want)
        print("relative err: ", err)
        print("abs err: ", abs(want-got))
        prior_relative_error += err
        prior_absolute_error += abs(want-got)
        print()
        
    prior_relative_error /= num_dimensions 
    prior_absolute_error/= num_dimensions 
    likelihood_relative_error /= num_dimensions 
    likelihood_absolute_error /= num_dimensions 

    print("average prior relative error: ", prior_relative_error)
    print("average prior absolute error: ", prior_absolute_error)

    print("average likelihood relative error: ", likelihood_relative_error)
    print("average likelihood absolute error: ", likelihood_absolute_error)

    print("Testing objective function gradients")

    # test optimization gradients
    from scipy.spatial.transform import Rotation as R
    T = np.eye(4)
    T[:3,:3] = R.from_quat([0.0,0.0,0.0,1.0]).as_matrix()
    T[:3, 3] = np.array([0, -0.8, 0.59])
    planner.update_T_matrix(T)

    likelihood_relative_error = 0.0
    likelihood_absolute_error = 0.0
    num_dimensions = 7 + 8 # arm + hand
    for i in range(num_dimensions):
        if i >= 7:
            continue
        d = np.zeros(num_dimensions)
        gradient_index = i
        d[gradient_index] = 1
        print("testing likelihood obj f gradient")
        eps = 1e-4

        q = np.random.rand(num_dimensions)
        q_plus = q + eps * d
        q_minus = q - eps * d

        f1 = planner.likelihood_obj_f(q_plus, voxel_np, object_dim_np)
        print("f1 (q+): ", f1)
        f2 = planner.likelihood_obj_f(q_minus, voxel_np, object_dim_np)
        print("f2 (q-): ", f2)
      
        # to save gradient
        f3 = planner.likelihood_obj_f(q, voxel_np, object_dim_np)
        print("f3 (q): ", f3)
        analytic_grad = planner.likelihood_obj_f_gradient(q, voxel_np, object_dim_np)
        print("analytic grad: ", analytic_grad[gradient_index])

        grad_fd = (f1-f2) / (2 * eps)
        print("grad FD: ", grad_fd)
 

        want = grad_fd
        got = analytic_grad[gradient_index]
        err = (want - got) / (want)
        print("relative err: ", err)
        print("abs err: ", abs(want-got))
        likelihood_relative_error += err
        likelihood_absolute_error += abs(want-got)
        print()


    likelihood_relative_error /= (num_dimensions -8 ) #don't care about the hand
    likelihood_absolute_error /= (num_dimensions - 8) 

    print("average likelihood relative error: ", likelihood_relative_error)
    print("average likelihood absolute error: ", likelihood_absolute_error)

    print()

if __name__ == "__main__":
    main()


