from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import JointState
from scipy.spatial.transform import Rotation as R
import numpy as np
import rospy


def convert_to_world_frame(palm_pose_obj_frame, obj_pose_in_world_ROS):
    assert len(palm_pose_obj_frame) == 6 # RPY

    obj_pose_in_world = np.array([obj_pose_in_world_ROS.position.x,
                                  obj_pose_in_world_ROS.position.y,
                                  obj_pose_in_world_ROS.position.z,
                                  obj_pose_in_world_ROS.orientation.x,
                                  obj_pose_in_world_ROS.orientation.y,
                                  obj_pose_in_world_ROS.orientation.z,
                                  obj_pose_in_world_ROS.orientation.w])

    r_obj_to_world = R.from_quat(obj_pose_in_world[3:]).as_matrix()

    trans_obj_to_world = obj_pose_in_world[:3]
    palm_trans_obj_frame = palm_pose_obj_frame[:3]
    palm_trans_world_frame = np.dot(r_obj_to_world, palm_trans_obj_frame) + trans_obj_to_world

    palm_rot_obj_frame = palm_pose_obj_frame[3:]
    palm_rot_of = R.from_euler("xyz", palm_rot_obj_frame).as_matrix()
    res = np.dot(r_obj_to_world, palm_rot_of)
    palm_orient_world_frame = R.from_matrix(res).as_euler("xyz")

    
    palm_pose_world = np.concatenate((palm_trans_world_frame, palm_orient_world_frame)).flatten()
    return palm_pose_world

def convert_to_ROS_posestamped(pose_np):
    assert len(pose_np) == 6 # RPY for orientation
    pose_ros = Pose()
    pose_ros.position.x = pose_np[0]
    pose_ros.position.y = pose_np[1]
    pose_ros.position.z = pose_np[2]

    rot_np_quat = R.from_euler("xyz", pose_np[3:]).as_quat()
    pose_ros.orientation.x = rot_np_quat[0]
    pose_ros.orientation.y = rot_np_quat[1]
    pose_ros.orientation.z = rot_np_quat[2]
    pose_ros.orientation.w = rot_np_quat[3]

    pose_stamped = PoseStamped()
    pose_stamped.pose = pose_ros

    return pose_stamped

def convert_to_ROS_jointstate(joints_q_np):
    assert len(joints_q_np) == 16 # Allegro
    position = joints_q_np.tolist()
    msg = JointState()
    msg.position = position
    return msg

def get_joints_bounds():
    bounds = get_arm_q_bounds() + get_allegro_bounds()
    assert len(bounds) == 7 + 8, bounds # arm + 2 joints per finger
    return bounds

def get_allegro_bounds():
    # page 3
    # http://wiki.wonikrobotics.com/AllegroHandWiki/images/e/e3/AllegroHandUsersManual.pdf
    palm_pose_bounds = [(-0.24, 0.24), # length of allegro hand 
                        (-0.24, 0.24),
                        (-0.24, 0.24),
                        (-3.15, 3.15), # [-pi,pi]
                        (-3.15, 3.15),
                        (-3.15, 3.15)]
    q_finger_bounds_upper = [0.57, 1.71, 1.80, 1.71]
    q_finger_bounds_lower = [-0.57, -0.29, -0.27, -0.32]
    q_thumb_bounds_upper = [1.49, 1.26, 1.74, 1.81]
    q_thumb_bounds_lower = [0.36, -0.20, -0.28, -0.26]

    #total = palm_pose_bounds -> useful when not optimizing over palm pose
    total = []

    # index, middle, ring
    for finger in range(3):
        for q_id in range(2): # only first two joints
            min_q = q_finger_bounds_lower[q_id] 
            max_q = q_finger_bounds_upper[q_id]
            total.append((min_q, max_q))

    # thumb
    for q_id in range(2): # only first two joints
        min_q = q_thumb_bounds_lower[q_id]
        max_q = q_thumb_bounds_upper[q_id]
        total.append((min_q, max_q))
    
    assert len(total) == 8, total
    return total

def get_arm_q_bounds():
    limits = [2.96,  # max 2.96 (rad) [170 deg]
              2.09,  # max 2.09 (rad) [120 deg]
              2.96,  # max 2.96 (rad) [170 deg]
              2.09,  # max 2.09 (rad) [120 deg]
              2.96,  # max 2.96 (rad) [170 deg]
              2.09,  # max 2.09 (rad) [120 deg]
              2.96]  # max 2.96 (rad) [170 deg]
    bounds = [(-1 * limit, limit) for limit in limits]
    assert len(bounds) == 7, bounds
    return bounds

def get_vector_from_skew_symmetric(matrix):
    return np.array([matrix[2][1], matrix[0][2], matrix[1][0]])

def get_skew_symmetric_from_vector(vector):
    assert len(vector) == 3
    m = np.zeros((3,3))

    m[0][1] = -vector[2]
    m[0][2] = vector[1]
    
    m[1][0] = vector[2]
    m[1][2] = -vector[0]
    
    m[2][0] = -vector[1]
    m[2][1] = vector[0]

    return m

def to_another_frame(pose, T):
    '''
    pose: 6D numpy array with x,y,z position and xyz Euler values.
    T: 4x4 numpy homogeneous transformation matrix from the frame in which pose is to the other frame
    '''
    assert len(pose) == 6, pose
    trans_other_frame = np.dot(T[:3,:3], pose[:3]) + T[:3,3]
    rot_other_frame = R.from_matrix(np.dot(T[:3,:3], R.from_euler("xyz", pose[3:]).as_matrix())).as_euler("xyz")
    return np.concatenate((trans_other_frame, rot_other_frame)).flatten()

def twist_to_another_frame(twist, T, T_inverse):
    '''
    Equation 3.76 in Lynch & Park
    '''
    w_b = twist[:3]
    v_b = twist[3:]

    V_b = np.zeros((4,4))
    V_b[:3,:3] = get_skew_symmetric_from_vector(w_b)
    V_b[:3,3] = v_b

    V_s = np.dot(np.dot(T, V_b), T_inverse)
    
    w_s = get_vector_from_skew_symmetric(V_s[:3,:3])
    v_s = V_s[:3,3]

    return np.concatenate((w_s,v_s))
    

def get_T_inverse(T):
    # obtain T inverse: http://vr.cs.uiuc.edu/node81.html
    # also: Lynch & Park 3.3.1.1; proposition 3.15
    rot = T[:3,:3]
    trans = T[:3,3]

    rot_4x4 = np.eye(4)
    rot_4x4[:3,:3] = rot.T 
    trans_4x4 = np.eye(4)
    trans_4x4[:3,3] = -1 * trans
    return np.dot(rot_4x4, trans_4x4)

def test_frame_conversion():
    print("This can be tested in Python 2.")
    '''
    To run this in Python 2, change 'matrix' to 'dcm' in R.matrix() calls


    import tf
    rospy.init_node("frame_conversion_test")

    # T is based off of object location
    object_frame = [0.0, -0.8, 0.59, 0.0, 0.0, 0.0, 1.0] # x,y,z, quats
    T = np.eye(4)
    T[:3,:3] = R.from_quat(object_frame[3:]).as_matrix()
    T[:3, 3] = np.array(object_frame[:3])

    # convert pose from one frame to another
    pose_obj_frame = np.random.rand(6)
    pose_world_frame = to_another_frame(pose_obj_frame,T)

    # test T inversion
    pose_obj_frame_computed = to_another_frame(pose_world_frame, get_T_inverse(T))

    # convert to quaternions and visualize
    pose_world_frame_euler = (pose_world_frame[3], pose_world_frame[4], pose_world_frame[5])
    pose_obj_frame_euler = (pose_obj_frame[3], pose_obj_frame[4], pose_obj_frame[5])
    pose_obj_frame_computed_euler = (pose_obj_frame_computed[3], pose_obj_frame_computed[4], pose_obj_frame_computed[5])
    pose_world_frame_quat = tf.transformations.quaternion_from_euler(pose_world_frame[3], pose_world_frame[4], pose_world_frame[5])
    pose_obj_frame_quat = tf.transformations.quaternion_from_euler(pose_obj_frame[3], pose_obj_frame[4], pose_obj_frame[5])
    pose_obj_frame_computed_quat = tf.transformations.quaternion_from_euler(pose_obj_frame_computed[3], pose_obj_frame_computed[4], pose_obj_frame_computed[5])

    br = tf.TransformBroadcaster()
    rate = rospy.Rate(10)

    # visualize in rviz
    rospy.loginfo("visualizing TFs.")
    rospy.loginfo("'pose_world_frame', 'pose_obj_frame', and 'pose_obj_frame_computed' should be the same")
    while not rospy.is_shutdown():
        br.sendTransform(object_frame[:3], object_frame[3:], rospy.Time.now(), "object_frame", "world")
        br.sendTransform(pose_world_frame[:3], pose_world_frame_quat, rospy.Time.now(), "pose_world_frame", "world")
        br.sendTransform(pose_obj_frame[:3], pose_obj_frame_quat, rospy.Time.now(), "pose_obj_frame", "object_frame")
        br.sendTransform(pose_obj_frame[:3], pose_obj_frame_computed_quat, rospy.Time.now(), "pose_obj_frame_computed", "object_frame")
        rate.sleep()
    '''

def test_skew_symmetric_matrix():
    v = np.random.rand(3)
    m = get_skew_symmetric_from_vector(v)
    v_2 = get_vector_from_skew_symmetric(m)
    np.testing.assert_array_equal(v, v_2)
    print("Skew symmetry conversion is correct")


def test_twist_to_another_frame():
    '''
    Lynch & Park Example 3.23
    '''
    twist = np.array([0,0,2,-2,-4,0])
    T = np.array([[-1, 0, 0, 4],
                  [0, 1, 0, 0.4],
                  [0, 0, -1, 0],
                  [0, 0, 0, 1]])
    T_inverse = get_T_inverse(T)

    twist_b = twist_to_another_frame(twist, T_inverse, T)
    np.testing.assert_array_equal(twist_b, [0.0,0.0,-2.0,2.8,4.0,0.0])
    print("twist frame conversion is correct")

def main():
    #test_frame_conversion()
    test_skew_symmetric_matrix()
    test_twist_to_another_frame()

if __name__ == "__main__":
    main()

