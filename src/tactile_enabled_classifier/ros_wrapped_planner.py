#!/usr/bin/env python3
import rospy
import sys
import torch
import roslib.packages as rp
import geometry_msgs.msg
from geometry_msgs.msg import Pose
import numpy as np
sys.path.append(rp.get_pkg_dir("grasp_pipeline"))
from grasp_pipeline.srv import *
from planner import Planner
from dnn_architecture_preshape import PreshapeNet
from dnn_architecture_in_hand import InHandNet
from dataset_loader import extract_hand_config
sys.path.append(rp.get_pkg_dir("prob_grasp_planner") + "/src/grasp_common_library")
from voxel_visualization_tools import convert_to_dense_voxel_grid, visualize_voxel
import roslib
roslib.load_manifest("prob_grasp_planner")
from prob_grasp_planner.srv import *
from utils import convert_to_world_frame, convert_to_ROS_posestamped, convert_to_ROS_jointstate, to_another_frame
from mdn import MDN
from scipy.spatial.transform import Rotation as R

class ROSPlanner():
    def __init__(self, init_node=True):
        if init_node:
            rospy.init_node("grasp_planner") 
            s = rospy.Service("grasp_tactile_plan", GraspPreshape, self.callback)
            r = rospy.Service("evaluate_grasp", GraspInfo, self.evaluate)
            q = rospy.Service("evaluate_in_hand", GraspInfo, self.evaluate_in_hand)

        model = PreshapeNet()
        model.load_state_dict(torch.load("/home/mmatak/dnn-preshape"))

        mdn = MDN()
        mdn.load_state_dict(torch.load("/home/mmatak/mdn-preshape"))
        mdn = mdn.to(mdn.device)

        self.planner = Planner(model, mdn)
         
        self.in_hand_dnn = InHandNet()
        self.in_hand_dnn.load_state_dict(torch.load("/home/mmatak/dnn-in-hand"))

        # used to cache inputs for in-hand classifier
        self.voxel_grid = None
        self.obj_dim = None
        self.palm_pose_np = None
        rospy.loginfo("Ready to plan some awesome grasps!")

    def evaluate_in_hand(self, req):
        print("req: ", req)
        allegro_full_joint_state = req.full_allegro_joint_state.position

        in_hand_likelihood_input = np.concatenate((self.palm_pose_np, allegro_full_joint_state))
        in_hand_log_likelihood, config_gradient = self.in_hand_dnn.evaluate_log_likelihood(in_hand_likelihood_input, self.voxel_grid, self.obj_dim, get_grad=True)

        resp = GraspInfoResponse()
        resp.in_hand_success_log_likelihood = in_hand_log_likelihood[1]
        resp.in_hand_gradient_q = config_gradient
        return resp
 
    def evaluate(self, req):
        rospy.loginfo("Evaluating grasp")
        obj = req.obj

        voxel_grid = self.get_voxel_grid(obj)
        self.voxel_grid = voxel_grid

        obj_dim = np.array([obj.width, obj.height, obj.depth])
        self.obj_dim = obj_dim

        palm_pose_obj_frame = req.palm_pose_obj_frame
        assert palm_pose_obj_frame.header.frame_id == "estimated_object_pose", print(req)
        palm_pose = palm_pose_obj_frame.pose
        position = [palm_pose.position.x, palm_pose.position.y, palm_pose.position.z]
        orientation_q = [palm_pose.orientation.x, 
                         palm_pose.orientation.y, 
                         palm_pose.orientation.z, 
                         palm_pose.orientation.w] 
        orientation = R.from_quat(orientation_q).as_euler("xyz")
        pose_np = np.concatenate((position, orientation))
        self.palm_pose_np = pose_np


        allegro_full_joint_state = req.full_allegro_joint_state.position
        
        # first two joints of every finger only
        idx = [0,1,4,5,8,9,12,13]
        allegro_input = [allegro_full_joint_state[i] for i in idx]

        preshape_likelihood_input = np.concatenate((pose_np,allegro_input))
        in_hand_likelihood_input = np.concatenate((pose_np, allegro_full_joint_state))

        in_hand_log_likelihood = self.in_hand_dnn.evaluate_log_likelihood(in_hand_likelihood_input, voxel_grid, obj_dim)
        preshape_log_likelihood = self.planner.evaluate_log_likelihood(preshape_likelihood_input, voxel_grid, obj_dim)


        resp = GraspInfoResponse()
        rospy.loginfo("preshape log likelihood: " + str(preshape_log_likelihood))
        rospy.loginfo("in hand log likelihood: " + str(in_hand_log_likelihood))

        resp.preshape_success_prob = np.exp(preshape_log_likelihood[1])
        resp.in_hand_success_prob = np.exp(in_hand_log_likelihood[1])
        return resp

    def callback(self, req):
        print("generating grasp :) ")
        obj = req.obj
        dim = np.array([obj.width, obj.height, obj.depth])
        if req.pcd_is_in_object_frame:
            voxel_grid = self.get_voxel_grid(obj, True)
        else:
            voxel_grid = self.get_voxel_grid(obj)

        self.planner.set_obj_pose(obj.pose)
        if req.pose_only:
            # object frame, orientation as RPY
            position, orientation = self.planner.get_target_pose_obj_frame(voxel_grid, dim)
            position = position.cpu()
            orientation = orientation.cpu()
            orientation = R.from_euler("xyz", orientation).as_quat()#xyzw

            # create ROS pose:
            pose = Pose()
            pose.position.x = position[0]
            pose.position.y = position[1]
            pose.position.z = position[2]
            pose.orientation.x = orientation[0]
            pose.orientation.y = orientation[1]
            pose.orientation.z = orientation[2]
            pose.orientation.w = orientation[3]

            resp = GraspPreshapeResponse()
            resp.success = True
            resp.target_pose_obj_frame = pose
            return resp

        # for collisions only, when frame is in corner of a mesh
        if req.use_true_mesh:
            self.planner.update_true_mesh_pose(req.true_mesh_pose)

        success = False
        for i in range(5):
            result, success = self.planner.plan_grasp(voxel_grid, dim)
            
            # visualization only
            arm_q = result[:7]
            hand_q = extract_hand_config(result[7:])

            rospy.wait_for_service("update_robot_state")
            proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
            vis_req = UpdateRobotStateRequest()
            vis_req.joint_state = np.concatenate((arm_q, hand_q))
            proxy(vis_req)
            rospy.loginfo("result visualized!")

            if success == True:
                break

        resp = GraspPreshapeResponse()
        resp.success = success
        if success:
            arm_q = result[:7]
            hand_q = extract_hand_config(result[7:])
            
            # visualization only
            rospy.wait_for_service("update_robot_state")
            proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
            vis_req = UpdateRobotStateRequest()
            vis_req.joint_state = np.concatenate((arm_q, hand_q))
            proxy(vis_req)
            rospy.loginfo("result visualized!")
 
            # convert to ROS data structures
            hand_q = convert_to_ROS_jointstate(np.array(hand_q))

            resp.allegro_joint_state = [hand_q]
            resp.arm_configuration = arm_q
        else:
            rospy.loginfo("planning failed")

        print("resp: ", resp)
        return resp

    def get_voxel_grid(self, segmented_object, pcd_is_in_object_frame=False):
        sparse_grid, _, dim = self.get_sparse_grid(segmented_object, pcd_is_in_object_frame)
        print("sparse grid: ", sparse_grid)
        voxel_grid = convert_to_dense_voxel_grid(sparse_grid, dim[0])
        return voxel_grid

    def get_sparse_grid(self, seg_object, pcd_is_in_object_frame):
        # this is basically voxel_gen_client method from data_proc_lib.py
        # rewritten for python 3
        # --> this is duplicate code :( 

        rospy.loginfo('Waiting for service gen_voxel_from_pcd.')
        rospy.wait_for_service('gen_voxel_from_pcd')
        rospy.loginfo('Calling service gen_voxel_from_pcd.')
        voxel_grid_dim = np.array([26, 26, 26])
        voxel_grid_full_dim = np.array([32, 32, 32])
        voxel_trans_dim = (voxel_grid_full_dim - voxel_grid_dim) // 2
        try:
            if pcd_is_in_object_frame:
                gen_voxel_proxy = rospy.ServiceProxy('gen_voxel_from_transformed_pcd', GenGraspVoxel)
            else:
                gen_voxel_proxy = rospy.ServiceProxy('gen_voxel_from_pcd', GenGraspVoxel)
            gen_voxel_request = GenGraspVoxelRequest()
            max_dim = np.max([seg_object.width, seg_object.height, seg_object.depth])
            voxel_size = max_dim / voxel_grid_dim

            gen_voxel_request.seg_obj_cloud = seg_object.cloud
            gen_voxel_request.voxel_dim = voxel_grid_dim
            gen_voxel_request.voxel_trans_dim = voxel_trans_dim
            gen_voxel_request.voxel_size = voxel_size

            gen_voxel_response = gen_voxel_proxy(gen_voxel_request) 
            return np.reshape(gen_voxel_response.voxel_grid, 
                        [int(len(gen_voxel_response.voxel_grid) / 3), 3]), \
                    voxel_size[0], voxel_grid_full_dim
        except rospy.ServiceException as e:
            rospy.loginfo('Service gen_voxel_from_pcd call failed: %s'%e)
        rospy.loginfo('Service gen_voxel_from_pcd is executed.')


def main():
    planner = ROSPlanner()
    rospy.spin()

if __name__ == "__main__":
    main()
