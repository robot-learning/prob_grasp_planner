import torch
import torch.nn as nn
import torch.nn.functional as F
from voxel_ae.voxel_encoder import VoxelEncoder


class PreshapeNet(nn.Module):
    def __init__(self):
        super(PreshapeNet, self).__init__()
        # inspired by Lu et al. IROS 2020 architecture

        # voxel (1,343)
        self.voxel_encoder = VoxelEncoder()
        self.voxel_encoder.load_state_dict(torch.load("/home/mmatak/voxel_encoder"))

        # object size + voxel
        self.fc1 = nn.Linear(346, 128)
        self.fc2 = nn.Linear(128, 64)

        # grasp configuration: 8 joints (first 2 of every finger) + palm pose (6 numbers)
        self.grasp_fc1 = nn.Linear(14, 14)
        self.grasp_fc2 = nn.Linear(14, 8)

        # object size + voxel + grasp configuration 
        self.fc3 = nn.Linear(72, 64)
        self.fc4 = nn.Linear(64, 32)
        self.fc5 = nn.Linear(32,2)


    def forward(self, sample):
        voxel = sample["voxel"]
        object_dim = sample["object_dim"]

        grasp_config = sample["grasp_config"] # [6 + 8]
        # 6 = location + orientation (euler)
        # 16 = 4 fingers 4 joints (order: index, middle, ring, thumb)
        grasp_config.float()

        x = self.voxel_encoder(voxel)

        # voxel + object dimensions
        x = torch.cat((x, object_dim), dim=1)
        x = self.fc1(x)
        x = F.elu(x)
        x = self.fc2(x)
        x = F.elu(x)

        # grasp configuration
        g = self.grasp_fc1(grasp_config)
        g = F.elu(g)
        g = self.grasp_fc2(g)
        g = F.elu(g)
   
        # grasp config + voxel + object dimensions
        x = torch.cat((x, g), dim=1)
        x = self.fc3(x)
        x = F.elu(x)
        x = self.fc4(x)
        x = F.elu(x)

        x = self.fc5(x) # 32 -> 2
        x = F.log_softmax(x, dim=1) # better numerical properties than vanilla softmax
        return x

def main():
    net = PreshapeNet()
    for param_tensor in net.state_dict():
        print(param_tensor)

if __name__ == "__main__":
    main()
