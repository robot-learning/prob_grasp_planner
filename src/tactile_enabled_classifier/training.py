import numpy as np
import torch
import torch.optim as optim
import torch.nn.functional as F
from dnn_architecture_preshape import PreshapeNet
from dnn_architecture_in_hand import InHandNet
from dataset_loader import TactileGraspDataset
from imbalanced_dataset_sampler import ImbalancedDatasetSampler
import matplotlib.pyplot as plt

class WeightedFocalLoss(torch.nn.Module):
    "Non weighted version of Focal Loss"
    def __init__(self, alpha=.1, gamma=2):
        super(WeightedFocalLoss, self).__init__()
        self.alpha = torch.tensor([alpha, 1-alpha]).cuda()
        self.gamma = gamma

    def forward(self, inputs, targets):
        BCE_loss = F.binary_cross_entropy_with_logits(inputs, targets, reduction='none')
        targets = targets.type(torch.long)
        at = self.alpha.gather(0, targets.data.view(-1))
        pt = torch.exp(-BCE_loss)
        F_loss = self.alpha*(1-pt)**self.gamma * BCE_loss
        return F_loss.mean()

#focal_loss = WeightedFocalLoss()

def train(model, device, train_loader, optimizer, epoch, weight):
    model.train()

    # freeze weights for encoder
    for name, param in model.named_parameters():
        if param.requires_grad and "voxel" in name:
            param.requires_grad = False

    avg_loss = 0
    for batch_idx, sample in enumerate(train_loader):
        for key in sample:
            sample[key] = sample[key].to(device)
        optimizer.zero_grad()
        output = model(sample)
        target = sample["target"]
        loss = F.nll_loss(output, target, weight)
        loss.backward()
        optimizer.step()
        if batch_idx % 5 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                    epoch, batch_idx * len(sample), len(train_loader),
                    100. * batch_idx / len(train_loader), loss.item()))
        avg_loss = avg_loss + loss.item()
    avg_loss /= len(train_loader)
    return avg_loss

def test(model, device, test_loader, weight=None):
    model.eval()
    test_loss = 0
    correct = 0
    if weight is None:
        weight = torch.tensor((1.0,1.0))
        weight = weight.to(device)

    with torch.no_grad():
        class_0_counter = 0
        class_1_counter = 0
        for sample in test_loader:
            for key in sample:
                sample[key] = sample[key].to(device)
            target = sample["target"]
            output = model(sample)
            test_loss += F.nll_loss(output, target, weight, reduction='sum').item()  # sum up batch loss
            #y = torch.zeros(output.shape[0], output.shape[1])
            #y[range(y.shape[0]), target] = 1
            #y = y.to(device)
            #test_loss += F.binary_cross_entropy_with_logits(output, y, weight)
            pred = output.argmax(dim=1, keepdim=True)  # get the index of the max log-probability
            if pred.item() == 0:
                class_0_counter += 1
            else:
                class_1_counter += 1

            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))
        
    return correct / len(test_loader.dataset), test_loss, class_0_counter, class_1_counter


def main():
    torch.manual_seed(2021)
    device = torch.device("cuda")
    preshape_training = True
    path = "/mnt/data_collection"

    if preshape_training:
        # preshape training
        train_dataset = TactileGraspDataset(path, True, percentage = .9) 
        test_dataset = TactileGraspDataset(path, True)
        model = PreshapeNet().to(device)
    else:
        # in-hand training
        train_dataset = TactileGraspDataset(path, False, percentage = .9) 
        test_dataset = TactileGraspDataset(path, False)
        model = InHandNet().to(device)

    test_dataset.exclude_files(set(train_dataset.files))
    test_loader = torch.utils.data.DataLoader(test_dataset)
    train_loader = torch.utils.data.DataLoader(
            train_dataset,
            batch_size=64)
            #sampler=ImbalancedDatasetSampler(train_dataset))
    
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    weight = torch.tensor((1.0, 1.0))
    weight = weight.to(device)
    scheduler = optim.lr_scheduler.StepLR(optimizer, 20, gamma=0.1)
    max_epoch = -1
    max_accuracy = -1
    nr_epochs = 100
    train_losses = np.zeros(nr_epochs)
    test_losses = np.zeros(nr_epochs)
    class_0_predictions = np.zeros(nr_epochs)
    class_1_predictions = np.zeros(nr_epochs)
    for epoch in range(1, nr_epochs):
        train_loss = train(model, device, train_loader, optimizer, epoch, weight)
        accuracy, test_loss, class_0_counter, class_1_counter = test(model, device, test_loader)
        accuracy_str = str(accuracy)
        if accuracy > max_accuracy:
            max_epoch = epoch
            max_accuracy = accuracy
            if preshape_training:
                torch.save(model.state_dict(), "/home/mmatak/dnn-preshape")# + accuracy_str + "-" + str(epoch))
            else:
                torch.save(model.state_dict(), "/home/mmatak/dnn-in-hand")# + accuracy_str + "-" + str(epoch))

        if train_loss < 1e-5:
            break

        if train_loss > 1.0:
            train_loss = 1.0

        # used for logging
        train_losses[epoch] = train_loss
        test_losses[epoch] = test_loss
        class_0_predictions[epoch] = class_0_counter
        class_1_predictions[epoch] = class_1_counter

        if epoch % 100 == 0:
            fig, ax = plt.subplots()
            plt.xlabel("epoch")
            plt.ylabel("loss (NLL)")
            ax.plot(train_losses, 'g--', label='Training loss')
            ax.plot(test_losses, 'b--', label='Test loss')
            legend = ax.legend(loc='upper center')
            legend.get_frame().set_facecolor('C0')
            plt.savefig("losses(lr=0.01decay20).jpg", format="jpg")
            plt.close()

            fig, ax = plt.subplots()
            plt.xlabel("epoch")
            plt.ylabel("# predicted samples")
            ax.plot(class_0_predictions, 'r.', label='negative grasps')
            ax.plot(class_1_predictions, 'g.', label='positive grasps')
            legend = ax.legend(loc='upper center')
            legend.get_frame().set_facecolor('C0')
            plt.savefig("predicted-classes(lr=0.001decay20).jpg", format="jpg")
            plt.close()

        scheduler.step()
    print("max accuracy: " + str(max_accuracy))
    print("max epoch: " + str(max_epoch))

    if preshape_training:
        torch.save(model.state_dict(), "/home/mmatak/dnn-preshape") 
    else:
        torch.save(model.state_dict(), "/home/mmatak/dnn-in-hand")

    fig, ax = plt.subplots()
    plt.xlabel("epoch")
    plt.ylabel("loss (NLL)")
    ax.plot(train_losses, 'g--', label='Training loss')
    ax.plot(test_losses, 'b--', label='Test loss')
    legend = ax.legend(loc='upper center')
    legend.get_frame().set_facecolor('C0')
    plt.savefig("losses(lr=0.001decay20).jpg", format="jpg")
    #plt.show()
    plt.close()

    fig, ax = plt.subplots()
    plt.xlabel("epoch")
    plt.ylabel("# predicted samples")
    ax.plot(class_0_predictions, 'r.', label='negative grasps')
    ax.plot(class_1_predictions, 'g.', label='positive grasps')
    legend = ax.legend(loc='upper center')
    legend.get_frame().set_facecolor('C0')
    plt.savefig("predicted-classes(lr=0.001decay20).jpg", format="jpg")
    #plt.show()
    plt.close()



if __name__ == "__main__":
    main()
