import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import rospy 
from scipy.optimize import minimize
from scipy.spatial.transform import Rotation as R
from geometry_msgs.msg import PoseStamped, Pose
from torch import optim
from utils import get_joints_bounds, get_allegro_bounds, to_another_frame, get_T_inverse, twist_to_another_frame, get_skew_symmetric_from_vector
from dataset_loader import extract_hand_config

from dnn_architecture_preshape import PreshapeNet
from mdn import MDN

import sys
import roslib.packages as rp
from prob_grasp_planner.srv import *
sys.path.append(rp.get_pkg_dir("prob_grasp_planner") + "/src/grasp_common_library")
from collision_checker import CollisionChecker
sys.path.append(rp.get_pkg_dir("ll4ma_util") + "/src/ll4ma_util")
sys.path.append(rp.get_pkg_dir("grasp_pipeline"))
from grasp_pipeline.srv import *
sys.path.append(rp.get_pkg_dir("tf_tools") + "/scripts")
from tf_client import TFClient


np.random.seed(2021)
#torch.manual_seed(0)

SDF_SERVICE_NAME = "get_sdf"
def object_sdf_and_gradient_fn(point):
    req = SDFQueryRequest(point)
    proxy = rospy.ServiceProxy(SDF_SERVICE_NAME, SDFQuery)
    resp = proxy(req)
    return resp.sdf, np.array([resp.gradient])


class Planner():
    def __init__(self, model, mdn):
        self.model = model
        self.mdn = mdn
        self.q_grad = None
        self.model.eval()
        self.mdn.eval()
        rospy.wait_for_service("arm_fk")
        self.arm_fk = rospy.ServiceProxy("arm_fk", ArmKDL)
        rospy.wait_for_service("arm_jacobian")
        self.arm_jacobian = rospy.ServiceProxy("arm_jacobian", ArmKDL)
        rospy.wait_for_service("arm_ik")
        self.arm_ik = rospy.ServiceProxy("arm_ik", ArmKDL)

        # relative importance in optimization
        #self.prior_weight = 0.95
        self.prior_weight = 1.0
        self.likelihood_weight = 1.0 - self.prior_weight

        self.T_object_to_world = None
        self.T_world_to_object = None

        # used by collision checker, only when we have access to full mesh
        self.T_world_to_true_mesh = None

        # visualization
        self.tf_client = TFClient()
        self.collision_checker = CollisionChecker(0.01)
        
        self.collision_checker.to_obj_frame_fn = lambda x: to_another_frame(x, self.T_world_to_object)

        self.object_sdf_and_gradient_fn = lambda point: object_sdf_and_gradient_fn(point)
        rospy.loginfo("Waiting for service: " + SDF_SERVICE_NAME)
        rospy.wait_for_service(SDF_SERVICE_NAME)

        self.in_collision_w_env_lambda = 10.0
        self.in_collision_w_obj_lambda = 10.0

        rospy.loginfo("Planner initialized successfully.")

    
    def evaluate_log_likelihood(self, grasp_config_np, voxel_np, object_dim_np):
        # _np means it has to be numpy object
        voxel = torch.from_numpy(voxel_np.astype('float32')) # shape: (32,32,32)
        voxel = voxel.unsqueeze(0) # 1 x 32 x 32 x 32
        voxel = voxel.unsqueeze(0) # 1 x 1 x 32 x 32 x 32

        object_dim = torch.from_numpy(object_dim_np.astype('float32')) # shape: (3,)
        object_dim = object_dim.unsqueeze(0) # 1 x 3

        grasp_config = torch.from_numpy(grasp_config_np.astype('float32')) # shape: (14,)
        grasp_config = grasp_config.unsqueeze(0) # 1 x 14

        # needed for planning the grasp
        grasp_config.requires_grad=True

        sample = {}
        sample["voxel"] = voxel
        sample["object_dim"] = object_dim 
        sample["grasp_config"] = grasp_config

        output = self.model(sample)
        success_log_prob = output[0][1] # 0th sample, target class 1

        self.model.zero_grad()
        success_log_prob.backward()
        self.q_grad = grasp_config.grad.numpy()[0] # [0] because it is only one sample being evaluated

        return output.detach().numpy()[0] # same as above

    def update_true_mesh_pose(self, mesh_pose):
        T = get_T_from_pose(mesh_pose)
        T_world_to_mesh = get_T_inverse(T)
        self.collision_checker.to_obj_frame_fn = lambda x: to_another_frame(x, T_world_to_mesh)
        rospy.loginfo("Warning: Using true mesh frame for collisions checking")

    def set_obj_pose(self, pose):
        T = get_T_from_pose(pose)
        self.update_T_matrix(T)
        self.collision_checker.to_obj_frame_fn = lambda x: to_another_frame(x, self.T_world_to_object)
        rospy.loginfo("object frame updated")

    def likelihood_obj_f(self, x, voxel, obj_dim):
        '''
        voxel: object representation
        obj_dim: object dimension
        '''
        likelihood_input = self.convert_to_likelihood_input(x)
        log_likelihood = self.evaluate_log_likelihood(likelihood_input, voxel, obj_dim)
        result = -log_likelihood[1] # 1 is index of success=True, minimize NLL
        return result

    def likelihood_obj_f_gradient(self, x, dummy=None, dummy_2=None):
        # L-BFGS-B method requires float64 encoding
        # https://github.com/scipy/scipy/issues/5832
        grad = -self.q_grad.astype('float64') # palm pose gradient and hand q gradient
        palm_pose_grad = grad[:6]
        arm_q_gradient = self.from_pose_gradient_to_arm_q_gradient(x, palm_pose_grad)
        gradient = np.concatenate((arm_q_gradient, grad[6:])) # arm gradient + hand joints gradient
        assert len(gradient) == 15, gradient
        return gradient

    def convert_to_likelihood_input(self, x):
        assert len(x) == 15, x # arm + hand_q
        arm_q = x[:7]
        hand_q = x[7:]
        resp = self.arm_fk(arm_q, None, None)
        palm_pose = resp.pose
        # converting to object frame
        palm_pose = to_another_frame(palm_pose, self.T_world_to_object)
        return np.concatenate((palm_pose, hand_q))

    def visualize(self, mdn_outputs):
        '''
        @mdn_outputs: list of (x,p) tuples where x is value and p is PDF
        '''
        mdn_sorted = sorted(mdn_outputs, reverse=True, key = lambda x:x[1])
        mdn_sorted = [x[0].cpu().detach().numpy() for x in mdn_sorted]
        palm_poses = [x[:6] for x in mdn_sorted]
        self.tf_client.delete_all_frames()
        for i, palm_pose in enumerate(palm_poses):
            self.tf_client.add_frame(palm_pose[:3], R.from_euler("xyz", palm_pose[3:]).as_quat(), str(i), "estimated_object_pose")
        input("visualized")

        
    def get_target_pose_obj_frame(self, voxel_np, object_dim_np):
        mdn_init, (pi, mean, L, L_d) = self.mdn.sample_np(voxel_np, object_dim_np, nr_samples=1)
        #TODO: check for collisions, reachability, etc.
        position = mdn_init[:3]
        orientation = mdn_init[3:6]
        return position, orientation

    def plan_grasp(self, voxel_np, object_dim_np, visualize_mdn = False):
        # dim[0] -> y axis
        # dim[1] -> x axis
        # dim[2] -> z axis
        self.collision_checker.update_box_dim(object_dim_np[1], object_dim_np[0], object_dim_np[2])
        # maximizes posterior 

        arm_q_init = []
        brown_table_height = 0.59 # 59cm
        white_table_height = 0.76 # 59cm
        table_height = white_table_height # 
        sdf = lambda robot_height: table_height - robot_height
        '''

        def obj_f(q, voxel_np, obj_dim_np, pi, mean, L, L_d):
            # obj function gets arm_q + hands_q as input
            neg_log_likelihood = self.likelihood_obj_f(q, voxel_np, obj_dim_np)
            # likelihood and MDN network get hand_pose + hands_q as input
            x = self.convert_to_likelihood_input(q) # FK
            x = torch.from_numpy(x).type(self.mdn.dtype).to(self.mdn.device)
            mdn_loss = self.mdn.loss_fn(pi, mean, L, L_d, x, True).cpu().detach().numpy()
            #print("[obj f] prior loss : ", mdn_loss)
            #print("[obj f] neg log likelihood: ", neg_log_likelihood)
            total = self.prior_weight * mdn_loss + self.likelihood_weight * neg_log_likelihood

            # check for collisions and add penalty
            full_q = np.concatenate((q[:7], extract_hand_config(q[7:])))

            # collision with env 
            _, depth = self.collision_checker.get_penetration_depth(sdf, full_q)
            if depth >= 0.0:
                total += self.in_collision_w_env_lambda * depth
                self.in_collision_w_env_lambda *= 2
            else:
                self.in_collision_w_env_lambda = 1.0


            # collision with object
            _,_, min_sdf= self.collision_checker.compute_sdf_collision(full_q, self.object_sdf_and_gradient_fn)
            if min_sdf > 0.0:
                self.in_collision_w_obj_lambda = 1.0
            else:
                total += self.in_collision_w_obj_lambda * abs(min_sdf)
                self.in_collision_w_obj_lambda *= 2

            return total

        def jac(q, voxel_np, obj_dim_np, pi, mean, L, L_d):
            # q == arm_q + some hands_q 
            likelihood_gradient = self.likelihood_obj_f_gradient(q)
            prior_gradient = self.prior_obj_f_gradient(q)
            prior_gradient *= self.prior_weight 
            likelihood_gradient *= self.likelihood_weight
            print("prior gradient:  ")
            print(prior_gradient)
            print("likelihood gradient:  ")
            print(likelihood_gradient)
            total = self.prior_weight * prior_gradient + self.likelihood_weight * likelihood_gradient

            full_q = np.concatenate((q[:7], extract_hand_config(q[7:])))

            # check for collisions with environment 
            link, depth = self.collision_checker.get_penetration_depth(sdf, full_q)
            if depth >= 0.0:
                rospy.loginfo("computing gradient for collision with table")
                rospy.loginfo("link in collision: " + str(link))
                J = self.collision_checker.get_Jacobians(q[:7])

                # if the hand is in collision -> move palm up
                if link not in self.collision_checker.get_arm_links_names():
                    rospy.loginfo("moving palm up because " + str(link) + " is in collision with env")
                    link = "palm_link"
                    rospy.wait_for_service("update_robot_state")
                    proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
                    vis_req = UpdateRobotStateRequest()
                    vis_req.joint_state = full_q
                    proxy(vis_req)
                    rospy.loginfo("result visualized!")

                    input("press enter to continue")

                J_link = J[link]
                poses = self.collision_checker.fk_arm_links(q[:7])
                pose = poses[link]
                pose_target = np.copy(pose)
                pose_target[2] += depth + 0.03
                gradient_collision = np.dot(J_link.T, pose_target - pose)
                gradient = np.zeros(len(q))
                gradient[:len(gradient_collision)] = gradient_collision 
                print("total gradient before collision: ")
                print(total)
                
                print("lambda: ")
                print(self.in_collision_w_env_lambda)

                print("collision gradient with lambda: ")
                print(self.in_collision_w_env_lambda * gradient)

                total += self.in_collision_w_env_lambda * gradient
                print("total gradient after collision: ")
                print(total)
            else:
                rospy.loginfo("not in collision with environment")

            ###### check for collisions with the object ##########

            links_gradients, max_sdf_link, _ = self.collision_checker.compute_sdf_collision(full_q, self.object_sdf_and_gradient_fn)
            if len(links_gradients) == 0:
                rospy.loginfo("not in collision with object")
                return total
            else:
                rospy.loginfo("in collision with object!")
                rospy.loginfo("links_gradients: " +  str(links_gradients))
                rospy.loginfo("worst link: " + str(max_sdf_link))

            J = self.collision_checker.get_Jacobians(q[:7])
            arm_links = self.collision_checker.get_arm_links_names()

            if max_sdf_link in arm_links:
                link = max_sdf_link
                J_link = J[link]
                # position is in collision, not orientation
                J_link=J_link[:3,:]
                gradient_collision = np.ravel(np.dot(J_link.T, links_gradients[link].T))
            else:
                link = "palm_link"
                J_link = J[link]
                poses = self.collision_checker.fk_arm_links(q[:7])
                pose = poses[link]
                pose_target = np.copy(pose)
                pose_target[2] += depth + 0.03
                gradient_collision = np.ravel(np.dot(J_link.T, pose_target - pose))

            gradient = np.zeros(len(q))
            gradient[:len(gradient_collision)] = gradient_collision 

            return total
       '''
        # visualization of the reachability box 
        #box_x = self.collision_checker.bounding_box_corner[0] * 2
        #box_y = self.collision_checker.bounding_box_corner[1] * 2
        #box_z = self.collision_checker.bounding_box_corner[2] * 2
        #self.tf_client.update_box_mesh("estimated_object_pose", box_x, box_y, box_z)

        while arm_q_init is None or len(arm_q_init) == 0:
            mdn_init, (pi, mean, L, L_d) = self.mdn.sample_np(voxel_np, object_dim_np, nr_samples=1)

            if visualize_mdn:
                self.visualize(mdn_init)
            mdn_init_np = mdn_init.cpu().detach().numpy()
            
            # check if palm pose is within object box, i.e. in collision
            if self.collision_checker.point_in_collision_with_object(mdn_init_np[:3]):
                arm_q_init = None
                continue

            # check if object is reachable, i.e. within a box
            if not self.collision_checker.object_reachable_from_the_point(mdn_init_np[:3]):
                #print("not reachable")
                #self.tf_client.add_frame(mdn_init_np[:3],[0,0,0,1], "mdn_output", "estimated_object_pose")
                #input("press enter to continue")
                #self.tf_client.delete_all_frames()
                arm_q_init = None
                continue

            # solve IK
            arm_q_init = self.get_q_from_mdn(mdn_init_np)
            if arm_q_init is None or len(arm_q_init) == 0:
                continue

            hand_q = extract_hand_config(mdn_init_np[6:])
            full_q = np.concatenate((arm_q_init,hand_q))
 
            # object box collision
            if self.collision_checker.q_in_collision_with_object_box(full_q):
                '''
                print("MDN initialized in collision with object-box")
                # visualization only
                self.collision_checker.visualize_collision_spheres(full_q, self.tf_client)
                rospy.wait_for_service("update_robot_state")
                proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
                req = UpdateRobotStateRequest()
                req.joint_state = full_q
                proxy(req)
                '''
                arm_q_init = None
                continue
                
            # table collision
            link, depth = self.collision_checker.get_penetration_depth(sdf,full_q)
            if depth >= 0.0:
                '''
                print("MDN initialized in collision with table")
                print("penetration depth: ", depth)
                # visualization only
                self.collision_checker.visualize_collision_spheres(full_q, self.tf_client)
                rospy.wait_for_service("update_robot_state")
                proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
                req = UpdateRobotStateRequest()
                req.joint_state = full_q
                proxy(req)
                '''
                arm_q_init = None
                continue
            '''
            else:
                print("MDN not in collision with table :)")
                # visualization only
                self.collision_checker.visualize_collision_spheres(full_q, self.tf_client)
                rospy.wait_for_service("update_robot_state")
                proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
                req = UpdateRobotStateRequest()
                req.joint_state = full_q
                proxy(req)
            '''
             
            # sdf object collision
            '''
            print("computing SDF collision..")
            _, closest_link, min_sdf = self.collision_checker.compute_sdf_collision(full_q, self.object_sdf_and_gradient_fn, debug=False)
            if min_sdf <= 0.0:
                print("MDN initialized in collision with object")
                print("closest link: ", closest_link)
                print("sdf: ", min_sdf)
                # visualization only
                rospy.wait_for_service("update_robot_state")
                proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
                req = UpdateRobotStateRequest()
                req.joint_state = full_q
                proxy(req)

                arm_q_init = None
            '''


        print("obtained q from mdn")


        # [gradients test] start
        '''
        for i in range(15):
            eps = 1e-3
            gradient_index = 3

            d = np.zeros(14)
            d[gradient_index] = 1
            q = np.random.rand(14)
            q_plus = q + eps * d
            q_minus = q - eps * d

            print("posterior gradient test")
            f1 = obj_f(q_plus, voxel_np, object_dim_np, pi, mean, L, L_d)
            f2 = obj_f(q_minus, voxel_np, object_dim_np, pi, mean, L, L_d)
            grad_fd = (f1-f2) / (2 * eps)
            print("grad FD: ", grad_fd)

            obj_f(q, voxel_np, object_dim_np, pi, mean, L, L_d)
            likelihood_gradient = -self.q_grad.astype('float64')
            prior_gradient = self.mdn.q_grad
            total = self.prior_weight * prior_gradient + self.likelihood_weight * likelihood_gradient
            print("autodiff grad: ", total[gradient_index])

            want = grad_fd
            got = total[gradient_index]
            err = (want - got) / (want)
            print("relative err: ", err)
            print("abs err: ", abs(want-got))
            print()
        raw_input()
        '''
        # [gradients test] end

        mdn_loss = self.mdn.loss_fn(pi, mean, L, L_d, mdn_init, False).cpu().detach().numpy()
        print("[prior] initial loss (lower the better): ", mdn_loss)
        mdn_init = mdn_init.cpu().detach().numpy()
        print("[log likelihood] initial (higher the better): ", self.evaluate_log_likelihood(mdn_init, voxel_np, object_dim_np))
        q_init = np.concatenate((arm_q_init, mdn_init[6:]))
#        print("[neg posterior] initial (lower the better): ", obj_f(q_init, voxel_np, object_dim_np, pi, mean, L, L_d))
        

        # visualization only
        rospy.wait_for_service("update_robot_state")
        proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
        req = UpdateRobotStateRequest()
        req.joint_state = full_q
        proxy(req)
        print("initial config robot state visualized")


        '''
        args=(voxel_np, object_dim_np, pi, mean, L, L_d)
        bounds = get_joints_bounds()
        
        res = minimize(fun=obj_f,
                       x0=q_init,
                       method="L-BFGS-B",
                       args=args,
                       jac=jac,
                       bounds=bounds,
                       options={"disp":True, "maxiter": 1})
        solution = res.x
        '''
        solution = q_init


        likelihood_input = self.convert_to_likelihood_input(solution)
        likelihood_input_torch = torch.from_numpy(likelihood_input).type(self.mdn.dtype).to(self.mdn.device) 
        prediction = self.mdn.loss_fn(pi, mean, L, L_d, likelihood_input_torch, False).cpu().detach().numpy()
        print("[prior] after optimization loss (lower the better): ", prediction)
        print("[log likelihood] optimization result (higher the better): ", self.evaluate_log_likelihood(likelihood_input, voxel_np, object_dim_np))
#        print("[neg posterior] after optimization (lower the better): ", obj_f(solution, voxel_np, object_dim_np, pi, mean, L, L_d))
        print()
        
        #full_q = np.concatenate((solution[:7], extract_hand_config(solution[7:])))
        '''
        _, depth = self.collision_checker.get_penetration_depth(sdf, full_q)
        success = True
        if depth >= 0.0:
            print("result is in collision with env!")
            success = False
        else:
            print("result is not in collision with env!")

        _, worst_link, min_sdf = self.collision_checker.compute_sdf_collision(full_q, self.object_sdf_and_gradient_fn, debug=False)
        if min_sdf < 0.0:
            print("result is in collision with object!")
            print("worst link: ", worst_link)
            user = input("press enter to continue or 's' to use it nevertheless")
            if user != 's':
                success = False
            else:
                success = True
        else:
            print("result is not in collision with the object!")
        '''
        success=True

        return solution, success
  

    def update_T_matrix(self, T):
        '''
        Homogeneous matrix between object and world. It is built off of object pose. 
        Look for 'test_frame_conversion()' in grasp_kdl_test.py for more info.

        When converting a pose from object frame to world frame, use T_object_to_world.
        When converting a pose from world frame to oobject frame, use T_world_to_object.
        '''
        assert T.shape == (4,4)
        self.T_object_to_world = T
        self.T_world_to_object = get_T_inverse(T)

    def prior_obj_f_gradient(self, x):
        grad = self.mdn.q_grad
        palm_pose_grad = grad[:6]
        arm_q_gradient = self.from_pose_gradient_to_arm_q_gradient(x, palm_pose_grad)
        gradient = np.concatenate((arm_q_gradient, grad[6:])) # arm gradient + hand joints gradient
        assert len(gradient) == 15, gradient
        return gradient

    def from_pose_gradient_to_arm_q_gradient(self, x, palm_pose_grad):
        '''
        Pose must be in object frame.
        '''
        arm_q = x[:7] # because decision variable is arm_q + hand_q
        J_flat = self.arm_jacobian(arm_q, None, None).jacobian
        J = np.reshape(np.array(J_flat), (6,7))

        # approach from grasp_planner
        J_transform = np.zeros((6,6))
        T = self.T_world_to_object
        R = T[:3,:3]
        J_transform[:3,:3] = R
        J_transform[3:,3:] = R

        p = T[:3,3]
        J_transform[3:,:3] = np.dot(get_skew_symmetric_from_vector(p),R)
        '''
        used to be:
        J = np.dot(J_transform, J)
        arm_q_gradient = np.dot(palm_pose_grad, J)
        '''
        #print("adjoint map: ")
        #print(J_transform)
        part_1 = np.dot(palm_pose_grad, J_transform)
        #print("gradient in world frame:")
        #print(part_1)
        #print("gradient in object frame:")
        #print(palm_pose_grad)
        arm_q_gradient = np.dot(part_1,J)
        
        # old approach
        # this conversion is tested and it is correct
        #palm_pose_grad_world = twist_to_another_frame(palm_pose_grad, self.T_object_to_world, self.T_world_to_object)
        #arm_q_gradient = np.dot(palm_pose_grad_world, J) # 1x6 . 6x7 = 7x1 
        assert len(arm_q_gradient) == 7, arm_q_gradient
        return arm_q_gradient.T

    # IK on MDN
    def get_q_from_mdn(self, mdn_output, visualize = False):
        # MDN outputs in object frame
        init_pose = mdn_output[:6]
        init_pose = to_another_frame(init_pose, self.T_object_to_world)
        if visualize:
            self.tf_client.delete_all_frames()
            self.tf_client.add_frame(init_pose[:3], R.from_euler("xyz", init_pose[3:]).as_quat(), "mdn_output_world_frame", "world")
            input("visualized")
        pose_stamped = PoseStamped()
        pose_stamped.pose = Pose()
        pose_stamped.pose.position.x = init_pose[0]
        pose_stamped.pose.position.y = init_pose[1]
        pose_stamped.pose.position.z = init_pose[2]
        quat = R.from_euler("xyz", init_pose[3:]).as_quat()
        pose_stamped.pose.orientation.x = quat[0]
        pose_stamped.pose.orientation.y = quat[1]
        pose_stamped.pose.orientation.z = quat[2]
        pose_stamped.pose.orientation.w = quat[3]
        req = ArmKDLRequest()
        req.pose_ik = pose_stamped
        return self.arm_ik(req).q_ik

    def maximize_likelihood(self, voxel_np, object_dim_np):
        args=(voxel_np, object_dim_np)

        arm_q_init = []
        while len(arm_q_init) == 0: 
            mdn_init, _ = self.mdn.sample_np(voxel_np, object_dim_np) # palm pose + hands_q
            mdn_init = mdn_init.cpu().detach().numpy()
            arm_q_init = self.get_q_from_mdn(mdn_init)
        print("[likelihood] initial likelihood (higher the better): ", self.evaluate_log_likelihood(mdn_init, voxel_np, object_dim_np))

        method = "L-BFGS-B"

        x_init = np.concatenate((arm_q_init, mdn_init[6:]))
 
        res = minimize(fun=self.likelihood_obj_f,
                       x0=x_init,
                       method=method,
                       args=args,
                       jac=self.likelihood_obj_f_gradient,
                       bounds=get_joints_bounds(),
                       options={"disp":True})

        resp = self.arm_fk(res.x[:7], None, None)
        palm_pose = resp.pose

        # converting to object frame
        palm_pose = to_another_frame(palm_pose, self.T_world_to_object)
        pose_and_hand_q = np.concatenate((palm_pose, res.x[7:]))
 
        print("[likelihood] likelihood after optimization (higher the better): ", self.evaluate_log_likelihood(pose_and_hand_q, voxel_np, object_dim_np))
        return res

    def maximize_prior(self, voxel_np, object_dim_np):
        '''
        Operates in Euclidian space, i.e. the decision variable is a pose and 8 joints.
        '''
        pose_and_hand_q, (pi, mean, L, L_d) = self.mdn.sample_np(voxel_np, object_dim_np)

        prediction = self.mdn.loss_fn(pi, mean, L, L_d, pose_and_hand_q, True).cpu().detach().numpy()
        print("[prior] initial prediction loss (lower the better): ", prediction)


        def obj_f(x,pi,mean,L, L_d):
            x = torch.from_numpy(x).type(self.mdn.dtype).to(self.mdn.device)
            return self.mdn.loss_fn(pi, mean, L, L_d, x, True).cpu().detach().numpy()

        q_bounds = get_allegro_bounds()
        jac = lambda q, pi, mean, L, L_d: self.mdn.q_grad
        args = (pi, mean, L, L_d)
        x_init = pose_and_hand_q.cpu().detach().numpy()
        bounds = [(None,None) for i in range(6)] + q_bounds

        res = minimize(fun=obj_f,
                       x0=x_init,
                       method="L-BFGS-B",
                       args=args,
                       bounds=bounds,
                       jac=jac,
                       options={"disp":False})

        x = torch.from_numpy(res.x).type(self.mdn.dtype).to(self.mdn.device) 
        prediction = self.mdn.loss_fn(pi, mean, L, L_d, x, False).cpu().detach().numpy()
        print("[prior] after optimization loss (lower the better): ", prediction)

        return res

def get_T_from_pose(pose):
    orientation = pose.orientation

    T = np.eye(4)
    T[:3,:3] = R.from_quat([pose.orientation.x,
                            pose.orientation.y,
                            pose.orientation.z,
                            pose.orientation.w]).as_matrix()
    T[:3,3] = np.array([pose.position.x, pose.position.y, pose.position.z])
    return T



def main():
        rospy.init_node("planner")
        model = PreshapeNet()
        model.load_state_dict(torch.load("/home/mmatak/dnn-preshape"))
        mdn = MDN()
        mdn = mdn.to(mdn.device)
        mdn.load_state_dict(torch.load("/home/mmatak/mdn-preshape"))
        planner = Planner(model, mdn)

        # pose of the object
        T = np.eye(4)
        T[:3,:3] = R.from_quat([0.0,0.0,0.0,1.0]).as_matrix()
        T[:3, 3] = np.array([0, -0.8, 0.62])
        planner.update_T_matrix(T)
        #pose = np.random.rand(6)
        #pose_world = to_another_frame(pose, planner.T_object_to_world)
        #pose_obj = to_another_frame(pose_world, planner.T_world_to_object)
        #np.testing.assert_allclose(pose, pose_obj)
        path = "test-objects/"
        test_objects = ["v8_fusion_strawberry_banana-10-02-2021--13-46-15.npy",
                        "vo5_split_ends_anti_breakage_shampoo-10-01-2021--22-49-43.npy",
                        "vo5_tea_therapy_healthful_green_tea_smoothing_shampoo-09-28-2021--22-57-51.npy",
                        "white_rain_sensations_ocean_mist_hydrating_conditioner-10-02-2021--23-53-22.npy",
                        "zilla_night_black_heat-10-04-2021--11-12-43.npy"]
        paths = [path + obj for obj in test_objects]
        object_dims=[[0.1139,0.1139,0.1139],
                     [0.1904,0.1904,0.1904],
                     [0.2010,0.2010,0.2010],
                     [0.2074,0.2074,0.2074],
                     [0.1620,0.1620,0.1620]]
        for i, path in enumerate(paths):
            voxel_np = np.load(path)
            object_dim_np = np.array(object_dims[i])

            # posterior 
            result = planner.plan_grasp(voxel_np, object_dim_np)

            # this works in Euclidian space (maybe)
            #result = planner.maximize_prior(voxel_np, object_dim_np)

            # this works
            #result = planner.maximize_likelihood(voxel_np, object_dim_np)
        

if __name__ == "__main__":
    main()
