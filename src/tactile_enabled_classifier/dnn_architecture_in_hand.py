import torch
import torch.nn as nn
import torch.nn.functional as F

from voxel_ae.voxel_encoder import VoxelEncoder

class InHandNet(nn.Module):
    def __init__(self):
        super(InHandNet, self).__init__()
        # inspired by Lu et al. IROS 2020 architecture

        # voxel (1,343)
        self.voxel_encoder = VoxelEncoder()
        self.voxel_encoder.load_state_dict(torch.load("/home/mmatak/voxel_encoder"))

        # object size + voxel
        self.fc1 = nn.Linear(346, 128)
        self.fc2 = nn.Linear(128, 64)


        # grasp configuration: 16 joints + palm pose (6 numbers)
        self.grasp_fc1 = nn.Linear(22, 22)
        self.grasp_fc2 = nn.Linear(22, 8)

        # object size + voxel + grasp configuration 
        self.fc3 = nn.Linear(72, 64)
        self.fc4 = nn.Linear(64, 32)
        self.fc5 = nn.Linear(32,2)


    def forward(self, sample):
        voxel = sample["voxel"]
        object_dim = sample["object_dim"]

        grasp_config = sample["grasp_config"] # [6 + 16]
        # 6 = location + orientation (euler)
        # 16 = 4 fingers 4 joints (order: index, middle, ring, thumb)
        grasp_config.float()

        x = self.voxel_encoder(voxel)

        # voxel + object dimensions
        x = torch.cat((x, object_dim), dim=1)
        x = self.fc1(x)
        x = F.elu(x)
        x = self.fc2(x)
        x = F.elu(x)

        # grasp configuration
        g = self.grasp_fc1(grasp_config)
        g = F.elu(g)
        g = self.grasp_fc2(g)
        g = F.elu(g)
   
        # grasp config + voxel + object dimensions
        x = torch.cat((x, g), dim=1)
        x = self.fc3(x)
        x = F.elu(x)
        x = self.fc4(x)
        x = F.elu(x)

        x = self.fc5(x) # 32 -> 2
        x = F.log_softmax(x, dim=1) # better numerical properties than vanilla softmax
        return x

    def evaluate_log_likelihood(self, grasp_config_np, voxel_np, object_dim_np, get_grad=False):
        # _np means it has to be numpy object
        voxel = torch.from_numpy(voxel_np.astype('float32')) # shape: (32,32,32)
        voxel = voxel.unsqueeze(0) # 1 x 32 x 32 x 32
        voxel = voxel.unsqueeze(0) # 1 x 1 x 32 x 32 x 32

        object_dim = torch.from_numpy(object_dim_np.astype('float32')) # shape: (3,)
        object_dim = object_dim.unsqueeze(0) # 1 x 3

        grasp_config = torch.from_numpy(grasp_config_np.astype('float32')) # shape: (22,)
        grasp_config = grasp_config.unsqueeze(0) # 1 x 22

        if get_grad:
            self.zero_grad()
            grasp_config.requires_grad=True
        else:
            grasp_config.requires_grad=False

        sample = {}
        sample["voxel"] = voxel
        sample["object_dim"] = object_dim 
        sample["grasp_config"] = grasp_config

        output = self(sample)
        if get_grad:
            output[0][1].backward()
            grad = grasp_config.grad
            # first 6 are palm pose, then come 16 joint values
            grad = grad.detach().numpy()[0]
            grad = grad[6:]

        output = output.detach().numpy()[0] # same as above

        if get_grad:
            return output, grad
        else:
            return output

