import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

from tqdm import tqdm
from torch.distributions.multivariate_normal import MultivariateNormal
from torch.distributions import Categorical
from dataset_loader import TactileGraspDataset
from voxel_ae.voxel_encoder import VoxelEncoder


class MDN(nn.Module):
    def __init__(self, n_gaussians=3, output_dim=14, gpu=True):
        super(MDN, self).__init__()
        self.n_gaussians = n_gaussians
        self.n_outputs = output_dim
        if gpu:
            self.dtype = torch.cuda.FloatTensor
            self.device = torch.device("cuda")
        else:
            self.dtype = torch.float32
            self.device = torch.device("cpu")

        # inspired by Lu et al. IROS 2020 architecture
        # voxel
        self.voxel_encoder = VoxelEncoder()
        self.voxel_encoder.load_state_dict(torch.load("/home/mmatak/voxel_encoder"))

        # object size + voxel
        self.fc1 = nn.Linear(346, 128)
        self.fc2 = nn.Linear(128, 32)

        last_layer_size = 32 # MDN parameters
        self.pi = nn.Linear(last_layer_size, self.n_gaussians)
        self.mu = nn.Linear(last_layer_size, self.n_gaussians * self.n_outputs) 

        # lower triangle of covariance matrix (below the diagonal)
        self.L = nn.Linear(last_layer_size, int(0.5 * self.n_gaussians * self.n_outputs * (self.n_outputs - 1)))
        # the diagonal of covariance matrix
        self.L_diagonal = nn.Linear(last_layer_size, self.n_gaussians * self.n_outputs)

        # for optimization purposes (check planner.py)
        self.q_grad = None


    def forward(self, sample):
        voxel = sample["voxel"]
        object_dim = sample["object_dim"]

        x = voxel

        x = self.voxel_encoder(voxel)

        # voxel + object dimensions
        x = torch.cat((x, object_dim), dim=1)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)
        x = F.relu(x) # (n, 32)

        pi = nn.functional.softmax(self.pi(x), -1)
        mu = self.mu(x).reshape(-1, self.n_outputs, self.n_gaussians)
        L_diagonal = torch.exp(self.L_diagonal(x)).reshape(-1, self.n_outputs, self.n_gaussians)

        # below the main diagonal
        L = self.L(x).reshape(-1, int(0.5 * self.n_outputs * (self.n_outputs - 1)), self.n_gaussians)

        return pi, mu, L, L_diagonal

    def loss_fn(self, pi, mu, L, L_d, target, save_input_gradient = False):
        if save_input_gradient:
            target.requires_grad = True
        result = torch.zeros(target.shape[0], self.n_gaussians).to(self.device)
        tril_idx = np.tril_indices(self.n_outputs, -1) # -1 because it's below the main diagonal
        diag_idx = np.diag_indices(self.n_outputs)

        for idx in range(self.n_gaussians):
            tmp_mat = torch.zeros(target.shape[0], self.n_outputs, self.n_outputs).to(self.device)
            tmp_mat[:, tril_idx[0], tril_idx[1]] = L[:, :, idx]
            tmp_mat[:, diag_idx[0], diag_idx[1]] = L_d[:, :, idx]
            try:
                mvgaussian = MultivariateNormal(loc=mu[:, :, idx], scale_tril=tmp_mat)
            except ValueError as ve:
                print("error: ")
                print(ve)
                print("loc:")
                print(mu[:,:,idx])
                print("mu:")
                print(mu)
            result_per_gaussian = mvgaussian.log_prob(target)
            result[:, idx] = result_per_gaussian + pi[:, idx].log()
        result = -torch.mean(torch.logsumexp(result, dim=1))

        # when optimizing over q using non-torch optimizer (check planner.py)
        if save_input_gradient:
            result.backward(retain_graph=True)
            self.q_grad = target.grad.cpu().numpy().astype('float64')

        return result

    def train_network(self, data_loader, optimizer, nepoch=200):
        self.train()
        lossHistory = []

        # freeze weights for encoder
        for name, param in self.named_parameters():
            if param.requires_grad and "voxel" in name:
                param.requires_grad = False

        for epoch in tqdm(range(nepoch)):
            for batch_idx, sample in enumerate(data_loader):
                for key in sample:
                    sample[key] = sample[key].type(self.dtype).to(self.device)
                optimizer.zero_grad()
                pi, mu, L, L_d = self(sample)
                loss = self.loss_fn(pi, mu, L, L_d, sample["grasp_config"])
                loss.backward()
                optimizer.step()

            if epoch != 0 and epoch % 10 == 0:
                print("epoch: ", epoch, "loss: ", loss.item())
                torch.save(self.state_dict(), "/home/mmatak/mdn-preshape-epoch" + str(epoch))

            lossHistory.append(loss.item())

        print("Training finished, final loss: {}".format(loss.item()))
        return lossHistory

    def sample(self, sample, nr_samples=1):
        '''
        Return one sample from the most impactful distribution.
        :param sample: single input, dictionary with numpy values
        '''
        sample = self.convert_to_torch(sample)
        pi, mean, L, L_diagonal = self(sample)
        return self.sample_from_mdn(pi, mean, L, L_diagonal, nr_samples), (pi, mean, L, L_diagonal)

    def convert_to_torch(self, sample):
        for key in sample:
            if type(sample[key]).__module__ == np.__name__:
                sample[key] = torch.from_numpy(sample[key]).type(self.dtype).to(self.device)
            elif type(sample[key]).__module__ == torch.__name__:
                sample[key] = sample[key].type(self.dtype).to(self.device)
            else:
                raise Exception("Value passed is neither numpy nor torch")
            sample[key] = sample[key].to(self.device)
        return sample

    def sample_from_mdn(self, pi, mean, L, L_diagonal, nr_samples):
        tril_idx = np.tril_indices(self.n_outputs, -1)
        diag_idx = np.diag_indices(self.n_outputs)
        
        torch_L = torch.zeros((self.n_outputs, self.n_outputs)).to(self.device)

        # [start] visualization only purposes
        '''
        for i in range(pi.shape[1]):
            gaussian_idx = i
            torch_L[tril_idx[0], tril_idx[1]] = L[0, :, gaussian_idx]
            torch_L[diag_idx[0], diag_idx[1]] = L_diagonal[0, :, gaussian_idx]

            mvgaussian = MultivariateNormal(loc=mean[0, :, gaussian_idx], scale_tril = torch_L)
        '''
        # [end] visualization only purposes

        # (probably) pick distribution
        mix = Categorical(pi)
        gaussian_idx = mix.sample().item()

        torch_L[tril_idx[0], tril_idx[1]] = L[0, :, gaussian_idx]
        torch_L[diag_idx[0], diag_idx[1]] = L_diagonal[0, :, gaussian_idx]
        mvgaussian = MultivariateNormal(loc=mean[0, :, gaussian_idx], scale_tril = torch_L)

        if nr_samples == 1:
            result = mvgaussian.sample()
            #result = mvgaussian.mean
            #x = x.cpu().numpy()
        else:
            result = []
            for i in range(nr_samples):
                x = mvgaussian.sample()
                log_pdf = mvgaussian.log_prob(x)
                result.append((x,log_pdf))

        return result

    def sample_np(self, voxel_np, obj_dim_np, nr_samples=1):
        sample = {}
        sample["voxel"] = np.expand_dims(voxel_np, axis=(0,1))
        sample["object_dim"] = np.expand_dims(obj_dim_np, axis=0)
        return self.sample(sample, nr_samples)

def train():
    net = MDN(n_gaussians=3)
    net = net.to(net.device)
    print("device used for training: ", net.device)
    optimizer = optim.Adam(net.parameters(), lr=0.001)
    path = "/mnt/data_collection/"

    train_dataset = TactileGraspDataset(path, positive_only=True, preshape_data=True) 
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=32)
    print('nr samples: ', len(train_dataset))
    losses = net.train_network(data_loader = train_loader, optimizer = optimizer)

    torch.save(net.state_dict(), "/home/mmatak/mdn-preshape")

def main():
    train()
 
    path = "/mnt/data_collection/"
    train_dataset = TactileGraspDataset(path, positive_only=True, preshape_data=True) 
    data_loader = torch.utils.data.DataLoader(train_dataset, batch_size=1)
 
    # all elements must be 0 or 1
    voxel = torch.zeros((1,1,32,32,32))
    obj_dim = torch.rand((1,3))
    sample = {}
    sample["voxel"] = voxel
    sample["object_dim"] = obj_dim
   
    net = MDN(n_gaussians=3)
    net.load_state_dict(torch.load("/home/mmatak/mdn-preshape"))
    net = net.to(net.device)
    net.eval()

    config = net.sample(sample)

if __name__ == "__main__":
    main()
