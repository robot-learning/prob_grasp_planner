import rospy
import os
import ast
import pypcd
import numpy as np
import roslib
roslib.load_manifest("prob_grasp_planner")
from prob_grasp_planner.srv import *
import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("tf_tools") + "/scripts")
from tf_client import TFClient
from geometry_msgs.msg import Point, Quaternion
sys.path.append(rp.get_pkg_dir("prob_grasp_planner") + "/src/grasp_common_library")
from voxel_visualization_tools import visualize_voxel, convert_to_dense_voxel_grid
from time import sleep

def get_voxel_grid(segmented_object):
    sparse_grid, _, dim = get_sparse_grid(segmented_object)
    voxel_grid = convert_to_dense_voxel_grid(sparse_grid, dim[0])
    return voxel_grid

def get_sparse_grid(seg_object):
    # this is basically voxel_gen_client method from data_proc_lib.py
    # rewritten for python 3
    # --> this is duplicate code :( 

    rospy.loginfo('Waiting for service gen_voxel_from_pcd.')
    rospy.wait_for_service('gen_voxel_from_pcd')
    rospy.loginfo('Calling service gen_voxel_from_pcd.')
    voxel_grid_dim = np.array([26, 26, 26])
    voxel_grid_full_dim = np.array([32, 32, 32])
    voxel_trans_dim = (voxel_grid_full_dim - voxel_grid_dim) // 2
    try:
        gen_voxel_proxy = rospy.ServiceProxy('gen_voxel_from_pcd', GenGraspVoxel)
        gen_voxel_request = GenGraspVoxelRequest()
        max_dim = np.max([seg_object["object width"], seg_object["object height"], seg_object["object depth"]])
        voxel_size = max_dim / voxel_grid_dim

        gen_voxel_request.seg_obj_cloud = seg_object["cloud"]
        gen_voxel_request.voxel_dim = voxel_grid_dim
        gen_voxel_request.voxel_trans_dim = voxel_trans_dim
        gen_voxel_request.voxel_size = voxel_size

        gen_voxel_response = gen_voxel_proxy(gen_voxel_request) 
        return np.reshape(gen_voxel_response.voxel_grid, 
                    [int(len(gen_voxel_response.voxel_grid) / 3), 3]), \
                voxel_size[0], voxel_grid_full_dim
    except rospy.ServiceException as e:
        rospy.loginfo('Service gen_voxel_from_pcd call failed: %s'%e)
    rospy.loginfo('Service gen_voxel_from_pcd is executed.')

def load_pointcloud(path):
    cloud = pypcd.PointCloud.from_path(path)
    msg = cloud.to_msg()
    msg.header.frame_id = "world"
    return msg

def get_ros_pose(pose):
    '''
    From array to ROS position and orientation
    '''
    position = Point()
    position.x = pose[0]
    position.y = pose[1]
    position.z = pose[2]
    orientation = Quaternion()
    orientation.x = pose[3]
    orientation.y = pose[4]
    orientation.z = pose[5]
    orientation.w = pose[6]
    return position, orientation

def main():
    rospy.init_node("blah")
    tf_client = TFClient()
    root_dir = "/mnt/data_collection"
    if not root_dir.endswith("/"):
            root_dir += "/"
    files = [root_dir + f for f in os.listdir(root_dir) if f.endswith(".txt")]
    published = False
    empty_counter = 0
    non_empty_counter = 0
    success_empty_counter = 0
    success_counter = 0
    unsuccessful_empty_counter = 0
    unsuccessful_counter = 0
    for i, f in enumerate(files):
        with open(f) as fp:
            parsing_object_pose = False
            pose = None
            sample = {}
            for line in fp:
                if "=" in line: 
                    key, value = line.split("=")
                else:
                    key = None
                    value = None

                if key in ("object width", "object height", "object depth"):
                    sample[key] = ast.literal_eval(value)
                if key == "success":
                    sample[key] = value
                if key is not None and "est_obj_pose" in key:
                    #parsing_object_pose = True
                    #pose = []
                    #line_nr = 0
                    #continue
                    #sample[key] = ast.literal_eval(value)
                    pose = ast.literal_eval(value)
                    pose = pose[0] + pose[1]
            '''
                if parsing_object_pose:
                    if "orientation" in line:
                        line_nr = 4
                        continue
                    pose.append(ast.literal_eval(line.split(": ")[1]))
                    line_nr += 1
                    if line_nr == 8:
                        parsing_object_pose = False
            '''
            position, orientation = get_ros_pose(pose)
            tf_client.update_object_pose(position, orientation, "estimated_object_pose")
            
            sample["cloud"] = load_pointcloud(f[:-4] + ".pcd")

            voxel = get_voxel_grid(sample)
            #visualize_voxel(voxel, as_cubes=True, dense_voxel_grid=True)
            if sum(sum(sum(voxel))) == 0.0:
                if sample["success"].strip() == "True":
                    success_empty_counter += 1
                else:
                    unsuccessful_empty_counter += 1
                empty_counter += 1
                np.save(f[:-4] + "-empty-voxel-grid", voxel)
            else:
                if sample["success"].strip() == "True":
                    success_counter += 1
                else:
                    unsuccessful_counter += 1
                non_empty_counter += 1
                np.save(f[:-4], voxel)
            print("processed: " + str(i+1) + " out of " + str(len(files)))
    print("total empty voxel grids: " + str(empty_counter))
    print("successful empty voxel grasps: " + str(success_empty_counter))
    print("unsuccessful empty voxel grasps: " + str(unsuccessful_empty_counter))
    print("total non-empty voxel grids: " + str(non_empty_counter))
    print("successful non-empty voxel grasps: " + str(success_counter))
    print("unsuccessful non-empty voxel grasps: " + str(unsuccessful_counter))

if __name__ == "__main__":
    main()
