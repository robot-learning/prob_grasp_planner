import torch
import os
import numpy as np
import ast
import random
from torch.utils.data import Dataset
from scipy.spatial.transform import Rotation as R


class TactileGraspDataset(Dataset):
    """Tactile Grasp dataset."""

    def __init__(self, root_dir, preshape_data, positive_only = False, percentage = None):
        """
        Args:
            preshape_data (bool): hand configuration used (in-hand vs preshape)
            root_dir (string): Directory with all the files.
            percentage (float): Percentage of the files to take from root_dir
        """
        if not root_dir.endswith("/"):
            root_dir += "/"
        all_files = set(os.listdir(root_dir))
        self.files = []
        for f in all_files:
            if f.endswith(".txt"):
                no_extension_path = os.path.splitext(f)[0]
                empty_voxel = no_extension_path + "-empty-voxel-grid.npy"
                if positive_only:
                    skip_sample = False
                    for line in open(root_dir + f):
                        if "success=False" in line:
                            skip_sample = True
                            break;
                    if skip_sample:
                        continue
                if empty_voxel in all_files:
                    continue
                else:
                    self.files.append(root_dir + f)
        self.used_properties = set(["object width",
                                    "object height",
                                    "object depth", 
                                    "palm_pose(est object frame)", 
                                    "hand_config_preshape",
                                    "hand_config_after_closing",
                                    "success"])

        if percentage is not None and percentage < 1.0:
            nr_files = int(percentage*len(self.files))
            self.files = random.sample(self.files, nr_files) 

        assert preshape_data is not None
        self.preshape_data = preshape_data
                    

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        full_path = self.files[idx]

        sample_raw = {}
        with open(full_path) as fp:
            for line in fp:
                if "=" not in line:
                    continue
                key, value = line.split("=")

                if key in self.used_properties:
                    if "palm_pose" in key:
                        tensor_value = parse_palm_pose(value)
                    else:
                        tensor_value = torch.as_tensor(ast.literal_eval(value))
                    sample_raw[key] = tensor_value 

        # associated voxel
        no_extension_path = os.path.splitext(full_path)[0]
        numpy_path = no_extension_path + ".npy"
        with open(numpy_path, "rb") as f:
            voxel = np.load(f)
            sample_raw["voxel"] = torch.from_numpy(voxel.astype('float32'))

        voxel, object_dim, grasp_config, target = prepare_data(sample_raw, self.preshape_data)
        sample = {"voxel": voxel,
                  "object_dim": object_dim,
                  "grasp_config": grasp_config,
                  "target": target}
        return sample

    def exclude_files(self, files_to_exclude):
        new_files = []
        for filename in self.files:
            if filename not in files_to_exclude:
                new_files.append(filename)
        self.files = new_files


def prepare_data(sample, preshape_data):
    voxel = sample["voxel"] # 32 x 32 x 32
    voxel = voxel.unsqueeze(0) # 1 x 32 x 32 x 32

    object_dim = torch.tensor([sample["object width"],
                               sample["object height"],
                               sample["object depth"]])

    position, orientation = sample["palm_pose(est object frame)"] # 3x1
    position = torch.squeeze(position) # 3,
    orientation = torch.squeeze(orientation) # 3,
    grasp_config = torch.cat((position, orientation)) # 6,

    if preshape_data:
        preshape_q = sample["hand_config_preshape"] # 16
        # first two joints of every finger
        preshape_q = torch.tensor(
                      [preshape_q[0],preshape_q[1],
                      preshape_q[4],preshape_q[5],
                      preshape_q[8],preshape_q[9],
                      preshape_q[12],preshape_q[13]]
                      )
        q = torch.squeeze(preshape_q) # 8,
    else:
        in_hand_q = torch.squeeze(sample["hand_config_after_closing"]) # 16,
        q = torch.squeeze(in_hand_q)

    grasp_config = torch.cat((grasp_config, q))

    target = sample["success"]
    target = target.to(torch.long)
    return voxel, object_dim, grasp_config, target

def extract_hand_config(solution):
    assert len(solution) == 8
    joints_q = solution
    q = [joints_q[0], joints_q[1], 0.0, 0.0,
         joints_q[2], joints_q[3], 0.0, 0.0,
         joints_q[4], joints_q[5], 0.0, 0.0,
         joints_q[6], joints_q[7], 0.0, 0.0]
    return q

def parse_palm_pose(value_string):
    value_numerical = ast.literal_eval(value_string)

    # position is saved as (x,y,z) location
    position = torch.tensor(value_numerical[0])

    # rotation is saved in quaternians -> not good for DNN input 
    # we transform it to euler representation
    r = R.from_quat(value_numerical[1])
    orientation_euler = r.as_euler("xyz")
    orientation = torch.from_numpy(orientation_euler.copy().astype('float32'))

    return position, orientation

def main():
    dataset = TactileGraspDataset("/mnt/data_collection/")
    print(len(dataset))
    for i, sample in enumerate(dataset):
        print(sample)
        if i == 10:
            break

if __name__ == "__main__":
    main()
