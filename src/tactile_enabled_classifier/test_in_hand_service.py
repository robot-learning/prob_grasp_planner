import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from ros_wrapped_planner import ROSPlanner
from dnn_architecture_in_hand import InHandNet
import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("grasp_pipeline"))
from grasp_pipeline.srv import *
from sensor_msgs.msg import JointState


def main():
    ros_planner = ROSPlanner(init_node=False)
   
    js = JointState()
    js.position = [0.1 for i in range(16)]

    voxel_np = np.ones((32,32,32))
    obj_dim = np.array([0.1,0.09,0.11])
    palm_pose = np.array([0.1,0.1,0.01,0,0,1])
    ros_planner.palm_pose_np = palm_pose
    ros_planner.obj_dim = obj_dim
    ros_planner.voxel_grid = voxel_np


    req = GraspInfoRequest()
    req.full_allegro_joint_state = js
    print("printed number should be getting smaller")
    for i in range(10):
        resp = ros_planner.evaluate_in_hand(req)
        js.position = np.array(js.position) - np.array(resp.in_hand_gradient_q)
        req.full_allegro_joint_state = js
        print(resp.in_hand_success_log_likelihood)


if __name__ == "__main__":
    main()

