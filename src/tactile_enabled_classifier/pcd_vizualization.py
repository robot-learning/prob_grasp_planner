import rospy
import pypcd
import sensor_msgs.msg

path = "/mnt/data_collection/poisson_proc.stl-09-14-2021--15-12-27.pcd" 
rospy.init_node("viz")

cloud = pypcd.PointCloud.from_path(path)
msg = cloud.to_msg()
msg.header.frame_id = "world"

pub = rospy.Publisher("pcd_loaded", sensor_msgs.msg.PointCloud2, queue_size=1)

rate = rospy.Rate(10)

while not rospy.is_shutdown():
    pub.publish(msg)
    rate.sleep()
