import torch
import torch.utils.data
import pandas as pd

class ImbalancedDatasetSampler(torch.utils.data.sampler.Sampler):
    def __init__(self, dataset):
        self.indices = list(range(len(dataset)))
        self.num_samples = len(self.indices)
        
        labels = self._get_labels(dataset)
        zeros = labels.count(0)
        ones = len(labels) - zeros
        percentage_zero = 1.0 * zeros / len(labels)
        percentage_one = 1.0 - percentage_zero
        weight_one = 1.0 - percentage_one
        weight_zero = 1.0 - percentage_zero

        weights = []
        for i in range(len(labels)):
            if labels[i] == 1:
                weights.append(weight_one)
            else:
                weights.append(weight_zero)

        self.weights = torch.DoubleTensor(weights)

    def _get_labels(self, dataset):
        labels = [sample["target"] for sample in dataset]
        return labels

    def __iter__(self):
        return (self.indices[i] for i in torch.multinomial(self.weights, self.num_samples, replacement=False))

    def __len__(self):
        return self.num_samples
       

